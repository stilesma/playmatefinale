import React, { Component } from 'react'
import {connect} from 'react-redux'

import {getChatMessages, socket, addMessageToState} from '../../../actions/chats'

import MessageInline from './MessageInline'
import './message.css'

import AppBar from '@material-ui/core/AppBar'
import { Toolbar, Typography} from '@material-ui/core'



class Messages extends Component {
  constructor(props){
    super(props)
    this.state = {
      message: '',
      socket: null,
      messages: [],
      status: '',
      submittedMsg: '',
      counter: 0
    }
      this.scrollDown = this.scrollDown.bind(this)
  }
 
   scrollDown(){
    const {container} = this.refs
    container.scrollTop = container.scrollHeight
  }

  componentWillReceiveProps(){
    if(this.state.counter === 0 ){
      this.setState({messages:this.props.messageslist,
         socket: this.props.sock,
         counter: this.state.counter++})
    }
    
  }

  componentDidMount(){
    if(this.state.counter === 0){
      this.setState({messages:this.props.messageslist}) 
    }
    
    this.scrollDown()
  }

  componentDidUpdate(prevProps, prevState){
     this.scrollDown()
  }
  

  componentWillUpdate(){
    if(this.state.socket !== null){
      this.state.socket.socketobj.onmessage = this.pushToState 
    }
  }

  pushToState = e => {
    let msgList = this.state.messages
    const data = JSON.parse(e.data)
    if(data.timestamp != this.state.submittedMsg.timestamp){
      msgList.push(data)
    }
    
    this.setState({messages:msgList})
  }

 
  handleSubmit = e => { 
    e.preventDefault()
    var {messages} = this.state
    var data = {
      timestamp: new Date().toISOString(),
      message: this.state.message,
      sender: this.props.user.phone_number,
      isTyping: false,
      status: 'sent',
    };
    messages.push(data)
    this.setState({messages: messages, submittedMsg:data, message:''})
    const conn = this.state.socket.socketobj
    conn.send(JSON.stringify(data))
    this.setState({status:"sent"})
    //this.sendMessage(data)
  //  console.log(this.state.messages)
  //  console.log(data)
   // const messagel = messages.append(data)
   // this.socket.send(JSON.stringify(data));
//  this.setState({messages:messagel})
  // this.setState({message:''})


  }



  render() {
    const phone = this.props.second.phone_number
    const {message} = this.state
    const {handleClick}= this.props
    
    const messages = this.state.messages
    
    const exp = messages.length > 0;
    return (
      <React.Fragment>
        <div ref = 'container'
             className='thread-container'
             style={{
                width:`100vw`,
                height:'75vh',
                position:'relative',
                   }}>
          <AppBar elevation={6} style={styles.Appbar}>
              <Toolbar>
                <Typography>
                  {this.props.second.name}
                </Typography>
              </Toolbar>
          </AppBar >
          
         <div className="thread">
            <div style={{
               backgroundColor:"#ffffff",
              width:`98vw`,
               height:400,
               position:'relative',
               }}>
            {
                     {exp}
                           ?
                    messages.map((msg, index)=>{
                    return ( 
                      <MessageInline key={index} chat={msg} /*  */  />
                           )
           }) 
                         :
                         
              <p style={{
                       marginLeft:'auto',
                       marginRight:'auto',
                       width:'50%',
                       fontSize:'30px',
                      
                       color:'#bdbdbd'}}>No Chat yet </p> 
            }
            </div>
            </div>
  </div>
              <div className="message-input">
                   <form onSubmit={this.handleSubmit}
                         className="message-form">
                      <input 
                          // onChange={this.handleChange} 
                          onChange = {
                            ({target}) =>{
                                this.setState({message:target.value, status:'typing'})
                 }
                }
                          name="message" 
                          className="form-control"
                          value={message} 
                          type="text"
                          autoComplete={'off'}
                          placeholder="Type a message"
                            /> 
                      <button 
                          type="submit"
                          className = "send"
                          disabled = {message.length<1}
                          >
                           Send
                      </button>
                    </form>
                  </div>
        
        
        
      </React.Fragment>
    )
  }
}

const styles = {
  Appbar:{
    position:"relative",
    background:" linear-gradient(0.25turn,#e91e63,#f06292)"
  }
}

const mapStateToProps = state => ({
  chatMessages: state.chats.messages,
  second: state.chats.secondUser,
  thread: state.chats.thread,
  user: state.auths.user,
  sock: state.chats.socket,
  chatlist: state.chats.chatlist,
  messageslist:state.chats.messages
})
export default connect(mapStateToProps, {getChatMessages, socket,addMessageToState})(Messages)
