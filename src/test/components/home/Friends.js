import React, { Component } from 'react'
import {connect} from 'react-redux'

import {getChatlist} from '../../actions/chats'
import FriendInline from './FriendInline'
import { CircularProgress } from '@material-ui/core';

class Friends extends Component {

  componentDidMount(){
    this.props.getChatlist()
  }
  
  render() {
    const {chatlist} = this.props
    const {display_name} = this.props

    return (
      <div>
        
          {
             chatlist.length > 0 ? chatlist.map((chat, index)=>{
              return ( 
                <FriendInline  key={index} info={chat.message}  user_name={display_name}  />
              )
            }) : 
            <div style={{
              marginLeft:'auto',
              marginRight:'auto',
              width:'50%',
              fontSize:'30px',
              marginTop:'50%',
              color:'#bdbdbd'
             }}>
            <p 
            >No Chat Yet</p>
            <CircularProgress  color='secondary'/>
            </div>
          }
    
      </div>
    )
  }
}

const styles = {
  MuiCircularProgress:{
    color:'secondary',
    marginLeft:'auto',
    marginRight:'auto',
  }
}

const mapStateToProps = state => ({
  chatlist: state.chats.chatlist
   
})
export default connect(mapStateToProps, {getChatlist})(Friends)
