import React, { Component } from 'react'
import './message.css'
import './Chat/chat.scss'

export default class Messages extends Component {
   constructor(props){
     super(props);
     this.state = {
       message: []
     }
     this.scrollDown = this.scrollDown.bind(this)
  }

  scrollDown(){
  const chat = this.messagesEnd;
  const scrollHeight = chat.scrollHeight;
  const height = chat.clientHeight;
  const maxScrollTop = scrollHeight -height;
  chat.scrollTop = maxScrollTop > 0 ? maxScrollTop : 0;
  }

   componentDidMount(){
    this.scrollDown()
  } 

   componentDidUpdate(prevProps,prevState){
    this.scrollDown()
  }  
  render() {
    let {message} = this.props
    const {messages,renderMessages} = this.props
    message = messages
    console.log(message && renderMessages(messages))
    return (
      <div ref='container'
           className="thread-container"
           style={{width:800,height:520}}    >
        <div className="thread">
          <div style={{backgroundColor:"#fff"}}>
            <ul ref= {(el) => {this.messagesEnd = el}}>
             {
               
               message && renderMessages(messages)
             }
           </ul>
           <div>{renderMessages}
           </div>
         </div>
     
       </div>
     </div>

   );
  }
}
