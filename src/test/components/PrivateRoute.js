import React from "react";
import { Route, Redirect } from "react-router-dom";
import { connect } from "react-redux";

const PrivateRoute = ({ component: Component, auth, ...rest }) => (
  
  
  <Route
    {...rest}
    render={props => {
      
      if (auth.isLoading) {
        return <h2>Loading...</h2>;
      }else if (auth.isAuthenticated ) {
        return <Component {...props} />;
      }else if(auth.validated === false && auth.isAuthenticated === false){
        return <Redirect to="/validatephone" />;
      }
      else {
        return <Component {...props} />;
      }
    }}
  />
);

const mapStateToProps = state => ({
  auth: state.auths
});

export default connect(mapStateToProps)(PrivateRoute);
