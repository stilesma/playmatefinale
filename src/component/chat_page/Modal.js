import React, {Component} from 'react'
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Profile from  '../register_page/Profile';
import Button from '@material-ui/core/Button'

export default class Modal extends Component{
    constructor(props){
        super(props);
        this.state = {
            open: false
        }
    }

    handleClose = () => {
        this.setState({ open: false });
      };


render(){
    const {open} = this.state
    return(
      <div>
        <Dialog
            open={open}
            onClose={this.handleClose}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-title">{"Profile"}</DialogTitle>
            <DialogContent>

              <Profile/>
            
            </DialogContent>
             <DialogActions>
               <Button onClick={this.handleClose} color="primary">
                  Back
               </Button>
               <Button onClick={this.submitToken} color="primary" autoFocus>
                  Message{"stiles"}
               </Button>
            </DialogActions>
        </Dialog>
      </div>
)
}
}