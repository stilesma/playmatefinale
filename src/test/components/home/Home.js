import React, { Component } from 'react'
import { connect } from "react-redux";

import PropTypes from "prop-types";
import {Tab,Tabs,} from "@material-ui/core"

import SideBar from './SideBar'
import Message from './chats/Messages'

import {logout} from '../../actions/auths'
import {SwipeableDrawer,IconButton,Grid,Typography} from '@material-ui/core';
import PeopleIcon from '@material-ui/icons/People'
import SettingsIcon from '@material-ui/icons/Settings'
import PersonIcon from '@material-ui/icons/Person'
import {Redirect} from 'react-router-dom';
import ChatIcon from '@material-ui/icons/Chat'
import MeetingRoomIcon from '@material-ui/icons/MeetingRoom'
import SwipeBar  from './swipe/SwipeBar';

function TabContainer(props){
  return(
    <Typography component="div"
                style={{padding: 0}}>
                  {props.children}
                </Typography>
  )
}
TabContainer.propTypes = {
    children:PropTypes.node.isRequired  
}
class Home extends Component {
   constructor(props){
  super(props)
  this.state = {
   left:false,
   right:false,
   value:0,

  } 
}
  static propTypes = {
    phone_number: PropTypes.string,
    display_name: PropTypes.bool
  }

  handleLogout = () => {
    this.props.logout()
  }



onCheck = (e) => {
 this.setState ({check:e.target.checked})
}

toggleDrawer = (side, open) => event => {
  if(event && event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')){
    return;
  }

  this.setState({...this.state, [side]: open});
}

openProfilePage = () => {
 
}

  render() {
    if(this.props.register === true){
      return(
        <Redirect to="/userdetails" />
      )
    }
    const {/* user */ phone_number} = this.props
    const {right,left} = this.state
    
    return(
      <div style={{
       /*  display:'flex',
        position:'relative',
        flexDirection:'row' */
        //width:`100vw`,
        heght:'80vh'
          }}>
      <Grid container>
     
      
      <Grid item xs={12} style={styles.grid}>
       <div>
     
        <IconButton style={styles.iconButton}
                    title="Profile" >
            <PersonIcon color="inherit"/>
        </IconButton>
        <IconButton style={styles.iconButton}
                    title="Settings">
            <SettingsIcon color="inherit"/>
        </IconButton>
        
        <IconButton onClick = {this.toggleDrawer('right',true)} 
                style={styles.iconButton}
                title="Chats"> 
           <ChatIcon  color = 'inherit'/>
        </IconButton>
        <IconButton onClick = {this.toggleDrawer('left',true)} 
                style={styles.iconButton}> 
           <PeopleIcon  color = 'inherit'/>
        </IconButton> 
   
    
    <IconButton onClick = {this.handleLogout} 
                title="Logout" 
                className="logout" 
                style={styles.iconButtonLogout}> 
                
                <MeetingRoomIcon color="inherit"/>
        </IconButton>
        </div>


          <div className = 'chat-room'
               style={{backgroundColor:"#ffffff"}}>               
                 <Message socket={true}/>
                 </div> 
           </Grid>
          </Grid>
      <SwipeableDrawer
              anchor="right"
              open={right}
              onClose={this.toggleDrawer('right', false)}
              onOpen={this.toggleDrawer('right', true)}
              >
          <div style={{
                      color:"#ffffff",
                      display:"flex",
                      flexDirection:"row",
                      position:'relative',
                      alignItems:"flex-start",
                      height:400
                        }}
                  role="presententation"
                  onClick={this.toggleDrawer(right, false)}
                  onKeyDown={this.toggleDrawer(right, false)}
              > 
                    <SideBar
                        //display_name={user.profile.display === null ? "New User" : user.profile.display}
                          phone_number={phone_number}
                          handlelogout = {this.handleLogout}/>
           </div>
       
        </SwipeableDrawer>
       

        <SwipeableDrawer
              anchor="left"
              open={left}
              onClose={this.toggleDrawer('left', false)}
              onOpen={this.toggleDrawer('left', true)}
              >
          <div style={{
                      color:"#ffffff",
                      display:"flex",
                      flexDirection:"row",
                      position:'relative',
                      alignItems:"flex-start",
                      height:560,
                     // width:"90vw"
                        }}
                  role="presententation"
                  onClick={this.toggleDrawer(left, false)}
                  onKeyDown={this.toggleDrawer(left, false)}
              > 
                    <SwipeBar/>
           </div>
       
        </SwipeableDrawer>
      </div>
   
    )
  }
}
const display='block'
const styles = {
  button:{
    marginLeft:'auto',
    display:'row',
    flexGrow:1,
    justifyContent:'flex-end'
  },
  paper:{
    height:"5vh",
    display:display,
    backgroundColor: "#000000",
    backgroundColor:"transparent",
    color: "#ffffff",
    width:'100vw',
    position:'relative'
  },
  iconButton:{
    marginRight:20,
    color:"#ffffff",
    position:'relative'
  },
  Appbar:{
    position:'relative',
    background:" linear-gradient(0.25turn, #e91e63, #f06292)"
  },
  grid:{
    flexBasis:0
  },
  iconButtonLogout:{
    color:"#ffffff",
    float:'right'
  }
}

 const mapStateToProps = state => ({
  phone_number: state.auths.phone_number,
  chatMessages: state.chats.messages,
  user: state.auths.user,
  register: state.auths.register,
})
export default connect(mapStateToProps, {logout})(Home)
