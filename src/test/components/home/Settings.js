import React from 'react';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import PhoneInput from 'react-phone-number-input';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import EditIcon from '@material-ui/icons/Edit'
import LensIcon from '@material-ui/icons/Lens'
import Slider from '@material-ui/lab/Slider'
import { AppBar, TextField,
         ListItemText,List,ListItem,ListItemSecondaryAction, 
         MenuItem, Divider, InputAdornment, IconButton, Switch, Button } from '@material-ui/core';
import {AGE,BODYTYPE,GENDER,CHAT_GENDER,HEIGHT} from '../auths/List'

const Styles = {
  root: {
    width: '100%',
    height:'100vh'
  },
  heading: {
    fontSize: '15px',
    flexBasis: '33.33%',
    flexShrink: 0,
  },
  secondaryHeading: {
    fontSize: '15px',
    color: 'secondary',
  },
  textfield:{
   width:200
  },
  appbar:{
    background:" linear-gradient(0.25turn, #e91e63, #f06292)",
    color:"#ffffff",
    height:50,
  },
  slide:{
    width:200,
  
  },
} 

export default function Settings() {
  const [expanded, setExpanded] = React.useState(false);
  const [disabled, setDisabled] = React.useState({
    enableDisplayname: true,
    enableFirstname: true,
    enableLastname: true,
    enableEmail: true,
    enableDescription: true,
    enableAge: true,
    enableHeight: true,
    enableGender: true,
    enableBodytype: true,
    enableChatgender: true,
    enableChatdistance: true,
  })
  const [user_details, setUserdetails] = React.useState({
     display_name: '',
     first_name: '',
     last_name: '',
     email: '',
     description:'',
     age:'18',
     height:'160',
     gender:'male',
     body_type:'slim',
     chat_distance:10,   
     chat_gender:'female',
     hookup:false,
     dating:false
  })
  const [phone_number, setPhonenumber] = React.useState(0)

  

  const handleChange = name => event => {
      setUserdetails({...user_details, [name]: event.target.value})
  }

  const handleSlide = (event, chat_distance) => {
   setUserdetails({...user_details, chat_distance})
  }

 const handleCheck = name => event =>{
   setUserdetails({...user_details, [name]:event.target.checked} )
 }





const handleEditEnable = name => event => {
  setDisabled({...disabled, [name]: event.target.value})
}

  const handleExpandChange = panel => (event, isExpanded) => {
    setExpanded(isExpanded ? panel : false);
  };

  return (
    <div style= {Styles.root} >
    <AppBar color='inherit' position="relative" elevation={3} style={Styles.appbar}>
        <Typography color="inherit" component="title">
            Settings
        </Typography>
    </AppBar>
      <br/>
      <form>
      <ExpansionPanel expanded={expanded === 'panel1'} onChange={handleExpandChange('panel1')}>
        <ExpansionPanelSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1bh-content"
          id="panel1bh-header"
        >
          <Typography  style={Styles.heading} >General Profile Settings</Typography>
          <Typography  style={Styles.secondaryHeading} >Personal Details</Typography>
        </ExpansionPanelSummary>
       
           <List>
               <ListItem>
                  <ListItemText primary="Display Name" style={Styles.text}/> 
                 <ListItemSecondaryAction>
                 <TextField variant='outlined'
                            value={user_details.display_name}
                            label="Display Name"
                            disabled={disabled.enableDisplayname}
                            onChange={handleChange('display_name')}
                            InputProps={{
                              endAdornment:  (
                                <InputAdornment position="end">
                                  <IconButton
                                       edge="end"
                                       onClick={handleEditEnable('enableDisplayname')}
                                       >
                                        <EditIcon/>
                                       </IconButton>
                                </InputAdornment>
                              )
                            }}
                       />
                 </ListItemSecondaryAction>
               </ListItem>
               
               <br/>
               <Divider variant="middle"/>
               <br/>
               <ListItem>
                  <ListItemText primary="First Name" style={Styles.text}/> 
                 <ListItemSecondaryAction>
                 <TextField variant='outlined'
                            value={user_details.first_name}
                            label="First Name"
                            disabled={disabled.enableFirstname}
                            onChange={handleChange('first_name')}
                            InputProps={{
                              endAdornment:  (
                                <InputAdornment position="end">
                                  <IconButton
                                       edge="end"
                                       onClick={handleEditEnable('enableFirstname')}
                                       >
                                        <EditIcon/>
                                       </IconButton>
                                </InputAdornment>
                              )
                            }}
                       />
                 </ListItemSecondaryAction>
               </ListItem>
               <br/>
               <Divider variant="middle"/>
               <br/>
               <ListItem>
                  <ListItemText primary="Last Name" style={Styles.text}/> 
                 <ListItemSecondaryAction>
                 <TextField variant='outlined'
                            value={user_details.last_name}
                            label="Last Name"
                            disabled={disabled.enableLastname}
                            onChange={handleChange('last_name')}
                            InputProps={{
                              endAdornment:  (
                                <InputAdornment position="end">
                                  <IconButton
                                       edge="end"
                                       onClick={handleEditEnable('enableLastname')}
                                       >
                                        <EditIcon/>
                                       </IconButton>
                                </InputAdornment>
                              )
                            }}
                       />
                 </ListItemSecondaryAction>
               </ListItem>
               <br/>
               <Divider variant="middle"/>
               <br/>
               <ListItem>
                  <ListItemText primary="E-mail" style={Styles.text}/> 
                 <ListItemSecondaryAction>
                 <TextField variant='outlined'
                            value={user_details.email}
                            label="E-mail"
                            type="email"
                            disabled={disabled.enableEmail}
                            onChange={handleChange('email')}
                            InputProps={{
                              endAdornment:  (
                                <InputAdornment position="end">
                                  <IconButton
                                       edge="end"
                                       onClick={handleEditEnable('enableEmail')}
                                       >
                                        <EditIcon/>
                                       </IconButton>
                                </InputAdornment>
                              )
                            }}
                       />
                 </ListItemSecondaryAction>
               </ListItem>
               <br/>
               <Divider variant="middle"/>
               <br/>
               <br/>
               <ListItem>
                  <ListItemText primary="Description" style={Styles.text}/> 
                 <ListItemSecondaryAction>
                 <TextField variant='outlined'
                            value={user_details.description}
                            label="Description"
                            multiline
                            rows="4"
                            disabled={disabled.enableDescription}
                            onChange={handleChange('description')}
                            InputProps={{
                              endAdornment:  (
                                <InputAdornment position="end">
                                  <IconButton
                                       edge="end"
                                       onClick={handleEditEnable('enableDescription')}
                                       >
                                        <EditIcon/>
                                       </IconButton>
                                </InputAdornment>
                              )
                            }}
                       />
                 </ListItemSecondaryAction>
               </ListItem>
               <br/>
               <br/>
               <Divider variant="middle"/>
               <br/>
               <br/>
               <ListItem>
                  <ListItemText primary="Age" style={Styles.text}/> 
                 <ListItemSecondaryAction>
                 <TextField value={user_details.age}
                            label="Age"
                            select
                            style={Styles.textfield}
                            disabled={disabled.enableAge}
                            onChange={handleChange('age')}
                            InputProps={{
                              startAdornment: <InputAdornment position="start">yrs</InputAdornment>,
                              endAdornment:  (
                                <InputAdornment position="end">
                                  <IconButton
                                       edge="end"
                                       onClick={handleEditEnable('enableAge')}
                                       >
                                        <EditIcon/>
                                       </IconButton>
                                </InputAdornment>
                              )
                            }}
                       >
                         {AGE.map(option => (
                                        <MenuItem key={option.value} 
                                                  value={option.value}>
                                                        {option.label}
                                                     </MenuItem>
                                                                  ))}
                       </TextField>
                 </ListItemSecondaryAction>
               </ListItem>
               <br/>
               <Divider variant="middle"/>
               <br/> <ListItem>
                  <ListItemText primary="Height" style={Styles.text}/> 
                 <ListItemSecondaryAction>
                 <TextField value={user_details.height}
                            label="Height"
                            select
                            style={Styles.textfield}
                            disabled={disabled.enableHeight}
                            onChange={handleChange('height')}
                            InputProps={{
                              startAdornment: <InputAdornment position="start">cm</InputAdornment>,
                              endAdornment:  (
                                <InputAdornment position="end">
                                  <IconButton
                                       edge="end"
                                       onClick={handleEditEnable('enableHeight')}
                                       >
                                        <EditIcon/>
                                       </IconButton>
                                </InputAdornment>
                              )
                            }}
                       >
                          {HEIGHT.map(option => (
                                  <MenuItem key={option.value} 
                                            value={option.value}>
                                                  {option.label}
                                  </MenuItem>
                                      ))}
                       </TextField>
                 </ListItemSecondaryAction>
               </ListItem>
               <br/>
               <Divider variant="middle"/>
               <br/>
               <ListItem>
                  <ListItemText primary="Gender" style={Styles.text}/> 
                 <ListItemSecondaryAction>
                 <TextField value={user_details.gender}
                            label="Gender"
                            select
                            style={Styles.textfield}
                            disabled={disabled.enableGender}
                            onChange={handleChange('gender')}
                            InputProps={{
                              endAdornment:  (
                                <InputAdornment position="end">
                                  <IconButton
                                       edge="end"
                                       onClick={handleEditEnable('enableGender')}
                                       >
                                        <EditIcon/>
                                       </IconButton>
                                </InputAdornment>
                              )
                            }}
                      >
                        {GENDER.map(option => (
                                <MenuItem key={option.value} 
                                          value={option.value}>
                                                {option.label}
                                </MenuItem>
                                        ))}
                      </TextField>
                 </ListItemSecondaryAction>
               </ListItem>
               <br/>
               <Divider variant="middle"/>
               <br/>
               <ListItem>
                  <ListItemText primary="Body Type" style={Styles.text}/> 
                 <ListItemSecondaryAction>
                 <TextField value={user_details.body_type}
                            label="Body Type"
                            select
                            disabled={disabled.enableBodytype}
                            onChange={handleChange('body_type')}
                            InputProps={{
                              endAdornment:  (
                                <InputAdornment position="end">
                                  <IconButton
                                       edge="end"
                                       onClick={handleEditEnable('enableBodytype')}
                                       >
                                        <EditIcon/>
                                       </IconButton>
                                </InputAdornment>
                              )
                            }}
                       >
                         {BODYTYPE.map(option => (
                                   <MenuItem key={option.value} 
                                             value={option.value}>
                                                   {option.label}
                                   </MenuItem>
                                   ))} 
                       </TextField>
                 </ListItemSecondaryAction>
               </ListItem>
               <br/>
               <Divider variant="middle"/>
               <br/>
               <Button>Save</Button>
           </List>
      </ExpansionPanel>
      <ExpansionPanel expanded={expanded === 'panel2'} onChange={handleExpandChange('panel2')}>
        <ExpansionPanelSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel2bh-content"
          id="panel2bh-header"
        >
          <Typography  style={Styles.heading} >Chat Preference</Typography>
          <Typography  style={Styles.secondaryHeading} >
          Preference
          </Typography>
        </ExpansionPanelSummary>
        <List>
          <ListItem>
            <ListItemText primary="Maximum Distance Of Chat"
                          secondary={`mi`}/>
            <ListItemSecondaryAction>
            <Slider value={user_details.chat_distance}
                    min={1}
                    max={100}
                    disabled={disabled.enableChatdistance}
                    step={1}
                    aria-labelledby="slider-icon"
                    onChange={handleSlide}
                    color="secondary"
                    style={Styles.slide}
                    thumb={<LensIcon style={{ color: '#2196f3' }} />}/>
              <IconButton title="Edit"
                          edge="end"
                          onClick={handleEditEnable('enableChatdistance')}>
                <EditIcon/>
              </IconButton>
            </ListItemSecondaryAction>
          </ListItem>
          <Divider variant='middle'/>
          <ListItem>
                  <ListItemText primary="Gender" style={Styles.text}/> 
                 <ListItemSecondaryAction>
                 <TextField value={user_details.gender}
                            select
                            style={Styles.textfield}
                            disabled={disabled.enableChatgender}
                            onChange={handleChange('gender')}
                            InputProps={{
                              endAdornment:  (
                                <InputAdornment position="end">
                                  <IconButton
                                       title="Edit"
                                       edge="end"
                                       onClick={handleEditEnable('enableChatgender')}
                                       >
                                        <EditIcon/>
                                       </IconButton>
                                </InputAdornment>
                              )
                            }}
                      >
                        {CHAT_GENDER.map(option => (
                                <MenuItem key={option.value} 
                                          value={option.value}>
                                                {option.label}
                                </MenuItem>
                                        ))}
                      </TextField>
                 </ListItemSecondaryAction>
               </ListItem>
        </List>
      </ExpansionPanel>
      <ExpansionPanel expanded={expanded === 'panel3'} onChange={handleExpandChange('panel3')}>
        <ExpansionPanelSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel3bh-content"
          id="panel3bh-header"
        >
          <Typography style={Styles.heading} >User Status</Typography>
          <Typography  style={Styles.secondaryHeading} >
            Status
          </Typography>
        </ExpansionPanelSummary>
        <List>
          <ListItem>
            <ListItemText primary="Dating"/>
            <ListItemSecondaryAction>
                <Switch checked={user_details.dating}
                        onChange={handleCheck('dating')}/>
            </ListItemSecondaryAction>
          </ListItem>
          <Divider variant="middle"/>
          <ListItem>
            <ListItemText primary="Hookup/Escort"/>
            <ListItemSecondaryAction>
            <Switch checked={user_details.hookup}
                    onChange={handleCheck('hookup')}/>
            </ListItemSecondaryAction>
          </ListItem>
        </List>
      </ExpansionPanel>
      </form>
      <form>
      <ExpansionPanel expanded={expanded === 'panel4'} onChange={handleExpandChange('panel4')}>
        <ExpansionPanelSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel4bh-content"
          id="panel4bh-header"
        >
          <Typography  style={Styles.heading} >Phone Number</Typography>
        </ExpansionPanelSummary>
        <List>
          <ListItem>
            <ListItemText  primary="Phone Number" secondary={
              <div>
                 <InputAdornment position="end">
                      <IconButton
                           title="Edit"
                           edge="end"
                           onClick={handleEditEnable('enableChatgender')}
                           >
                            <EditIcon/>
                           </IconButton>
                    </InputAdornment>
              </div>
            }/>
            <ListItemSecondaryAction>
            <PhoneInput
                className='phone'
                //style={styles.phone}
                //placeholder="Enter phone number"
                value={ phone_number}
              
                onChange={ phone_number => setPhonenumber( phone_number ) } 
                />
           
               
            </ListItemSecondaryAction>
          </ListItem>
        </List>
      
    
      </ExpansionPanel>
      </form>
    </div>
  );
}