import React, { Component } from 'react'
import SpeedDial from '@material-ui/lab/SpeedDial'
import SpeedDialAction from '@material-ui/lab/SpeedDialAction'
import PersonIcon from '@material-ui/icons/Person';
import SaveIcon from '@material-ui/icons/Save';
import PrintIcon from '@material-ui/icons/Print';
import EditIcon from '@material-ui/icons/Edit';
import SpeedDialIcon from '@material-ui/lab/SpeedDialIcon';
import { Toolbar, Button } from '@material-ui/core';
import MediaCard from './InitChat/MediaCard';


const actions = [
  { icon: <SaveIcon />, name: 'Save' },
  { icon: <PrintIcon />, name: 'Print' },
  { icon: <PersonIcon />, name: 'Pofile' },

];
export default class SearchList extends Component {
  state = {
    open: false,
  };
  handleClick = () => {
    this.setState(state => ({
      open: !state.open,
    }));
  };

  handleOpen = () => {
    if (!this.state.hidden) {
      this.setState({
        open: true,
        open1: false
      });
    }
  };


  handleClick = (e) => {
    this.setState({ open1: true });
 
  };

  handleClose = () => {
    this.setState({
      open: false,
    });
  };
  render() {
    const { open } = this.state;
    
    return (
       <div>
          <div style={{width:800, marginLeft:"auto",height:585 ,position:"relative", backgroundColor:"#ffffff"} }>
              <div style={{height:400 ,}}>
                 <MediaCard/>
              </div>
              <Toolbar>
              <Button 
              style={styles.button}>Like</Button>
              <Button 
              style={styles.button}>Nope</Button>
            </Toolbar>
              <Toolbar>
              <SpeedDial
                 ButtonProps=        {{ color: "secondary" }}
                 direction="right"
                 
          ariaLabel="SpeedDial openIcon example"
          icon={<SpeedDialIcon openIcon={<EditIcon />} />}
          onBlur={this.handleClose}
          onClick={this.handleClick}
          onClose={this.handleClose}
          onFocus={this.handleOpen}
          onMouseEnter={this.handleOpen}
          onMouseLeave={this.handleClose}
          open={open}
          style={styles.speedial}
        >
          {actions.map(action => (
            <SpeedDialAction
              key={action.name}
              icon={action.icon}
              tooltipTitle={action.name}
              onClick={this.handleClick}
            />
          ))}
        </SpeedDial>
        </Toolbar>
      
        </div>  
       </div>
    )
  }
}
const styles = {
    appbar:{
        background:" linear-gradient(0.25turn, #e91e63, #f06292)",
        height:70,
        width:800
    },
    speedial:{
      marginTop:0,
      marginRight:"auto",
     // background:" linear-gradient(0.25turn, #e91e63, #f06292)",
     position:"relative"
    },
    button:{
      background:" linear-gradient(0.25turn, #e91e63, #f06292)",
      margin:20,
      width: 100,
      height:70,
      borderRadius:40,
      marginRight:"auto",
      marginLeft:"auto"
    }
  }
