import axios from 'axios';
import {
  AUTH_ERROR, 
  VALIDATE_SUCCESS,
  VALIDATE_FAILURE,
  LOGIN_SUCCESS,
  LOGIN_FAILURE,
  LOGOUT_SUCCESS,
  LOGOUT_FAILURE,
  REGISTER_SUCCESS,
  REGISTER_FAILURE,
  USER_LOADING,
  USER_LOADED,
  ADD_USER_INFO_FAILURE,
  ADD_USER_INFO_SUCCESS,
  IMAGE_UPLOAD_FAILURE,
  IMAGE_UPLOAD_SUCCESS,
} from './types';

const ORIGIN = '192.168.8.100'

export const loadUser = () => (dispatch, getState) => {
    dispatch({type: USER_LOADING})
    if (!getState().auths.token){
        return dispatch({
            type: AUTH_ERROR
        })
    }
  
    axios.get(`http://${ORIGIN}:8000/api/accounts/user/`, tokenConfig(getState))
        
  
        .then(res => {
            dispatch({
                type: USER_LOADED,
                payload:res.data[0]
            })
            
        }).catch(err => {
            dispatch({type:AUTH_ERROR})
        })
  }


export const validatePhone = payload => dispatch => {
  
  const config = {
      headers: {
          'Content-Type': 'application/json'
      }
  }

  // Request body
  const body = JSON.stringify({"phone_number":payload['phone_number']})

  axios.post(`http://${ORIGIN}:8000/api/accounts/validate-phone/`, body, config)

      .then(res => {
          console.log(res);
          
          dispatch({
              type: VALIDATE_SUCCESS,
              payload:payload
          });
          
      }).catch(err => {
          dispatch({type:VALIDATE_FAILURE})
      })
}

export const login = (phone_number, otp) => dispatch => {
  const config = {
    headers: {
        'Content-Type': 'application/json'
    }
  }

  // Request body
  const body = JSON.stringify({phone_number, otp})

  axios
  .post(`http://${ORIGIN}:8000/api/accounts/login/`, body, config)
      .then(res => {
          dispatch({
              type: LOGIN_SUCCESS,
              payload:res.data
          })
      }).catch(err => {
          dispatch({type:LOGIN_FAILURE})
      })
}

export const register = (phone_number, otp) => dispatch => {
  const config = {
    headers: {
        'Content-Type': 'application/json'
    }
  }

  // Request body
  const body = JSON.stringify({phone_number, otp})
  console.log(body)
  axios
  .post(`http://${ORIGIN}:8000/api/accounts/register/`, body, config)
      .then(res => {
          console.log(res);
          
          dispatch({
              type: REGISTER_SUCCESS,
              payload:res.data
              
          })
      }).catch(err => {
          dispatch({type: REGISTER_FAILURE})
      })
}

export const logout = () => (dispatch, getState) => {
  dispatch({type: USER_LOADING})
    if (!getState().auths.token){
        return dispatch({
            type: AUTH_ERROR
        })
    }
    axios.post(`http://${ORIGIN}:8000/api/accounts/logout/`,null , tokenConfig(getState))
        .then(res => {
            dispatch({
                type: LOGOUT_SUCCESS,
            })
        }).catch(err => {
          dispatch({
            type: LOGOUT_FAILURE,
          })
        })
}

export const addUserInfo = (userDetails) => dispatch => {
    const config = {
        headers: {
            'Content-Type': 'application/json'
        }
      }

      const body = JSON.stringify({userDetails})
    
    axios.put(`http://${ORIGIN}/api`,body,config)
    .then(res => {
        console.log(res)

        dispatch({
            type:ADD_USER_INFO_SUCCESS,
            payload:res.data
        })
        .catch(err => {
            dispatch({
                type:ADD_USER_INFO_FAILURE
            })
        })
    })
}

export const updateInfo = userDetails => (dispatch) => {
    dispatch({
        
    })
}

export const addImage = ({image,title,content}) => dispatch => {
    const config = {
        headers: {
            'Content-Type': 'application/json'
        }
      }

      const body = JSON.stringify({image,title,content})
axios.post(`http:${ORIGIN}/api`,body,config)
     .then(res =>{

         dispatch({
             type:IMAGE_UPLOAD_SUCCESS,
             payload:res.data
         }).catch(err => {
             dispatch({
                 type:IMAGE_UPLOAD_FAILURE
             })
         })
     })
}

export const tokenConfig = getState => {
  const token = getState().auths.token
  const config = {
      headers: {
          'Content-Type': 'application/json'
      }
  }
  if(token){
      config.headers['Authorization'] = `Token ${token}`
  }
  return config
}