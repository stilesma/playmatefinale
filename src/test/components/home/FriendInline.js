import React, { Component } from 'react'
import {connect} from 'react-redux'
import {Redirect} from 'react-router-dom';

import {getChatMessages, getThread,socket, sendSocketToState, loadSocket} from '../../actions/chats'
import { Avatar, ListItem, ListItemAvatar,List, ListItemText, Typography,Divider } from '@material-ui/core';
import Messages from './chats/Messages'
import './chats/message.css'

class FriendInline extends Component {
   constructor(props){
     super(props)
     this.state= {
       activeChat:[]
     }
   }
  openChat = () => {
    if(this.props.user.id === this.props.info.thread.second.id){
      this.props.getChatMessages(this.props.info.thread.first.id)
      this.connectToSocket(this.props.info.thread.first.id)
    }else {
      this.props.getChatMessages(this.props.info.thread.second.id)
      this.connectToSocket(this.props.info.thread.second.id)
    }
    this.props.getThread(this.props.info.thread.id)
  
  }

  connectToSocket = (id) => {
    for(let socket of this.props.sockets){
      if (socket.secondUser === id)
      {
        this.props.loadSocket(socket)
        break
      }
    }
  }


 componentDidMount(){
  const user = this.props.user
  const thread = this.props.info.thread
  const secondUser =  thread.second.id === user.id ? thread.first : thread.second
  this.setState({secondUser:secondUser, activeChat:secondUser})
 const socket = this.props.socket(secondUser.id,user.id)
 this.props.sendSocketToState(secondUser.id, socket, this.props.sockets)

} 
  render() {
    const { display_name,  id,/* phone_number */} = this.props.info.thread.second.profile
   const { message, timestamp, status, user/* image, file, seen */ } = this.props.info
    const {user_name} = this.props
    
    const classname =this.state.activechat === this.props.info.thread.second.id ? 'active' : ''
    var time= new Date (timestamp)

    var hour = time.getHours()
    
  var min = time.getMinutes()
   
    var timer = `${hour}: ${min}`

   const  threadId  = this.props.info.id
    // Use the ID to link to the profile 
    return (
       <div onClick={this.openChat}>
        <div
        key ={id}
             className ={ `user ${classname}`}
             onClick = {this.openChat}
             style={{color:'#e91e63'}}
             >
              <List>
                 <ListItem alignItems="flex-start" 
                           className = "user-info">
        <ListItemAvatar>
          <Avatar>Mb</Avatar>
        </ListItemAvatar>
        <ListItemText
          style={styles.list}
          primary={display_name}
          secondary={
            <React.Fragment>
              <Typography
                component="span"
                variant="body2"
                style={styles.inline}
                color="textPrimary"
              >
                {message}
                
              </Typography>
              <Typography
                component="span"
                variant="body2"
                style={styles.timline}
                color="textPrimary"
              >
             {timer}
                
              </Typography>
              <Typography
                component="span"
                variant="body2"
                style={styles.siline}
                color="textPrimary"
              >
                {status}
                
              </Typography>       
            </React.Fragment>
          }
        />
      </ListItem>
      <Divider variant="middle" component="li" />
                </List>
            </div>
       {/*   <ol>
          <li>{user.phone_number}</li>
          <li>{display_name}</li>
          <li>{message}</li>
          <li>{timestamp}</li>
          <li>{status}</li>
    </ol>  */}
      </div> 
    
    )
  }
}
const styles = {
  paper:{
      width:450,
      marginLeft:'auto',
      marginRight:'auto',
      display:'inline',
  },
  inline:{
    display:'inline',
    color:'#e91e63'
  },

  timline: {
    display:'inline',
    marginLeft:20,
    color:'#e91e63'
  },
  siline: {
    marginLeft:'auto',
    color:'#e91e63'
  },
 list:{
  color:'#e91e63'
 }


}

const mapStateToProps = state => ({
  chatlist: state.chats.chatlist,
  user: state.auths.user,
  sockets: state.chats.sockets
})
export default connect(mapStateToProps, {sendSocketToState, getChatMessages, getThread, socket, loadSocket})(FriendInline)
