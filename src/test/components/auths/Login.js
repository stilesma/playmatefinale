import React, { Component } from 'react'

import {connect} from 'react-redux'
import {Redirect} from "react-router-dom"
import 'react-phone-number-input/style.css'
import PropTypes from 'prop-types'

import {login} from '../../actions/auths'
import TextField from '@material-ui/core/TextField'
import PhoneInput from 'react-phone-number-input'
import './phone.css'
import AppBar from '@material-ui/core/AppBar'
import Button from '@material-ui/core/Button'
import Paper from '@material-ui/core/Paper'
import Toolbar from '@material-ui/core/Toolbar'
import Typography  from '@material-ui/core/Typography'

class Login extends Component {
  static propTypes = {
    login: PropTypes.func.isRequired,
    isAuthenticated: PropTypes.bool,
    phone_number: PropTypes.string,
    validated: PropTypes.bool
  }


   state = {
     phone_number: '',
     otp: '',
     open: false,
     open1: false,
   }

   onSubmit = e => {
     e.preventDefault()
     this.props.login(this.state.phone_number, this.state.otp)
   }

   handleClickOpen = () => {
   this.setState({ open: true })
   };

   handleClickOpen1 = (e) => {
    this.setState({ open1: true });
    this.setState({open: false})
    
  };

  handleClose = () => {
    this.setState({ open: false });
  };

   handleChange = e => {
    
      const re = /^[0-9\b]+$/;
  
      // if value is not blank, then test the regex
  
      if (e.target.value === '' || re.test(e.target.value)) {
         this.setState({otp: e.target.value})
      }

   }

   componentDidMount(){
     this.setState({phone_number: this.props.phone_number})
   }

   render() {
      if(this.props.isAuthenticated){      
      return <Redirect to="/" />
    }
    if(this.props.validated === false){
      return <Redirect to="/validatephone" />
    }
    if(this.props.register){
      return <Redirect to="/register" />
    }  
     const {otp} = this.state
     return (
       <div>
          <AppBar position="static" style={styles.appbar}>
              <Toolbar>
                 <Typography variant="h3" style={styles.typo}>
                 Login
                 </Typography>
              </Toolbar>
          </AppBar>
      <Paper style={styles.paper}>
          <form onSubmit={this.onSubmit}>
            {/* <label htmlFor="phone_number">Phone Number</label> */}
                 <PhoneInput
                       name="phone_number"
                       className='phone'
                       style={styles.phone}
                       placeholder="Enter phone number"
                       value={this.props.phone_number }/> 
           {/* <input name="phone_number" type="tel" value={this.props.phone_number} readonly/>  */}
               <br/>
             <TextField name="otp"
                        onChange={this.handleChange}
                        value={otp}
                        variant="outlined"
                        placeholder="OTP"
                        label="OTP"/>
           {/*     
             <input
              type="number"
              name="otp"
              onChange={this.handleChange}
              value={otp}
              /> */}
              <br/>
              
             {/*  <button type="submit" onClick={this.handleClickOpen} className="btn btn-primary">
              Submit
            </button> */}
            <Button style={styles.button} type="submit" >Submit</Button>
           {/*   <Dialog
               open={this.state.open}
               onClose={this.handleClose}
               aria-labelledby="alert-dialog-title"
               aria-describedby="alert-dialog-description">
               <DialogTitle id="alert-dialog-title">{"Validate"}</DialogTitle>
                   <DialogContent>
                      <DialogContentText id="alert-dialog-description">
                         {`Do you want to continue with this number ${this.props.phone_number}`}
                      </DialogContentText>
                   </DialogContent>
                   <DialogActions>
                     <Button onClick={this.handleClose} color="primary">
                          Back
                     </Button>
                     <Button onClick={this.handleClickOpen1} color="primary" autoFocus>
                         Continue
                    </Button>
                   </DialogActions>
            </Dialog>
            <Dialog
               open={this.state.open1}
               onClose={this.handleClose}
               aria-labelledby="alert-dialog-title"
               aria-describedby="alert-dialog-description">
               <DialogTitle id="alert-dialog-title">{"Confirmation"}</DialogTitle>
                  <DialogContent>
                      <TextField 
                          inputProps={{"aria-valuemax":6,"aria-valuemin":6}}
                          name="otp"
                          onChange={this.handleChange}
                          value={otp} />
                  </DialogContent>
                  <DialogActions>
                      <Button onClick={this.handleClose} color="primary">
                            Back
                      </Button>
                      <Button type="submit" color="primary" autoFocus>
                           Continue
                      </Button>
                 </DialogActions>
            </Dialog> */} 
           </form>
         </Paper>
       </div>
     )
   }
 }
 
 const styles ={
  phone:{
    position:'relative',
    width:260,
    backgroundColor:'none',
    display:'inline-block',
    marginTop: 50,
  },
  paper:{
    width: 300,
    height: 300,
    marginLeft: 'auto',
    marginRight: 'auto',
    textAlign: 'center',
    marginTop: 100,
   
  },
  button:{
    marginTop:30,
    background:" linear-gradient(0.25turn, #e91e63, #f69d3c)",
    borderRadius:20,
    color:"inherit",
    width:200,
    height:40,
    fontFamily: 'georgia',
    fontWeight: "bold",
    fontSize:20,
    padding:-10
  },
  appbar:
  {
    color:"inherit",
    background:" linear-gradient(0.25turn, #e91e63, #f06292)" ,
    fontWeight: "bold",
    fontSize:30,
    fontStretch:10,
  },
  typo:{
    color:"#ffffff",
    marginLeft:'auto',
    marginRight:'auto'
  }
 }

 const mapStateToProps = state => ({
  isAuthenticated: state.auths.isAuthenticated,
  phone_number: state.auths.phone_number,
  validated: state.auths.validated,
  register: state.auths.register
})
export default connect(mapStateToProps, {login})(Login)