import {combineReducers} from 'redux'
import {user} from './userReducer'
import {upload} from './uploadReducer'
import {chat} from './chatReducer'

const rootReducer = combineReducers({
   user,
   upload,
   chat
})

export default rootReducer