import React, { Component } from 'react'
import './message.css'
import {connect} from 'react-redux'
import { ListItem, List, Paper, Typography,Divider,ListItemAvatar,ListItemText } from '@material-ui/core';

class MessageInline extends Component {
  render() {
    const {message, timestamp, seen, user} = this.props.chat
    
    const {display_name, phone } = this.props
    const user_phone_number = this.props.owner.phone_number
    
    const receiver_phone_number = user === undefined ? this.props.chat.sender : user.phone_number
    
    var time= new Date(timestamp)
    var hour = time.getHours()
   
    var min = time.getMinutes()
     
      var time = hour
    return (
      //this should confirm the owner if it is me, then display on the right hand side
      
      <List>
      <ListItem alignItems='flex-start'>
      <Paper elevation ={4}  
             color='inherit'
             style={receiver_phone_number === user_phone_number ? styles.messageright : styles.message}
              >
      <ListItemText
secondary={
  <React.Fragment>
  
    <Typography
      component="span"
      variant="body2"
      color="textPrimary"
    >
   {message}  
    </Typography>

    <Typography
      style={styles.time}
      component="span"
      variant="body2"
      color="textPrimary"
    >
      {seen}
    </Typography>
    <Typography
     // style={styles.time}
      component="span"
      variant="body2"
      color="textPrimary"
    >
      {timestamp}

    </Typography>
  </React.Fragment>
 
}
/>

      </Paper>
      </ListItem>

      </List>
   /*    <div 
        className={`message-container `}>
          <div className="time"> 12:12</div>
              <div className="data">
                   <div className="message">hey</div>
                   <div className="name">by mubi 098665743</div>
              </div>
      </div> */
      /* <div //this should confirm the sender if it is me, then display on the right hand side
         className={`message-container ${user.profile.display_name===display_name && 'right'}`}>
        <p>
          {message} 
          <p>{timestamp}</p>
          <p>{seen}</p>
        </p>
        <p>by {user.profile.display_name} {user.phone_number}</p> 
      </div> */

      


    )
  }
}
const styles = {
  time: {
    float: 'right'
  },
message:{
 float:'left',
 maxWidth:200
},
messageright:{
 marginLeft:'auto',
 backgroundColor:'#e91e63',
 color:"#ffffff",
 maxWidth:200
}
}
const mapStateToProps = state => ({
  owner: state.auths.user,
  second: state.chats.thread
 
})
export default connect(mapStateToProps)(MessageInline)