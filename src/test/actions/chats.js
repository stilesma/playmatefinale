import axios from 'axios';
import {
  AUTH_ERROR,
  THREAD_LOADED,
  THREAD_LOADING,
  THREAD_LOADING_FAIL,

  CHATLIST_LOADED,
  CHATLIST_LOADING,
  CHATLIST_LOADING_FAIL,

  MESSAGE_LOADED,
  MESSAGE_LOADING,
  MESSAGE_LOADING_FAIL,

  SECOND_USER_LOADED,
  SECOND_USER_LOADING,
  SECOND_USER_LOADING_FAIL,

  ONOPEN,
  ONCLOSE,
  ONMESSAGE,
  ADD_SOCKET_TO_STATE,

  ADD_MESSAGE_TO_STATE
} from './types'


import {tokenConfig} from './auths.js'//1 
import { log } from 'util';


export const ORIGIN = '192.168.8.100'// '192.168.43.104'


export const getThread = id => (dispatch, getState) => {
  dispatch({type: THREAD_LOADING})
  if (!getState().auths.token){
      return dispatch({
          type: AUTH_ERROR
      })
  }
  axios.get(`http://${ORIGIN}:8000/api/chats/thread/${id}/details/`, tokenConfig(getState))
  .then(res => {
      dispatch({
          type: THREAD_LOADED,
          payload: res.data
      })
  }).catch(err => {
    dispatch({
      type: THREAD_LOADING_FAIL,
    })
  })
}

export const getChatlist = () => (dispatch, getState) => {
  dispatch({type: CHATLIST_LOADING})
  
  
  if (!getState().auths.token){
    return dispatch({
        type: AUTH_ERROR
    })
  }
  axios.get(`http://${ORIGIN}:8000/api/chats/chat-list/`, tokenConfig(getState))

  .then(res => {
      dispatch({
          type: CHATLIST_LOADED,
          payload: res.data
      })
  }).catch(err => {
    dispatch({
      type: CHATLIST_LOADING_FAIL,
    })
  })
}

export const getChatMessages = id => (dispatch, getState) => {
  dispatch({type: MESSAGE_LOADING})
  dispatch({type: SECOND_USER_LOADING})
  if (!getState().auths.token){
    return dispatch({
        type: AUTH_ERROR
    })
  }
  // the id is the thread id
  axios.get(`http://${ORIGIN}:8000/api/chats/${id}/messages/`, tokenConfig(getState))
  .then(res => {
      
      dispatch({
          type: MESSAGE_LOADED,
          //payload:'hey'
          payload: res.data
      })
      dispatch ({
        type: SECOND_USER_LOADED,
        payload:res.data[0].thread.second
      })
  }).catch(err => {
    dispatch({
      type: MESSAGE_LOADING_FAIL,
    })
    dispatch({
      type: SECOND_USER_LOADING_FAIL,
    })
  })
}

export const socket = (pk,id) => (dispatch, getState) =>{
  let path = `ws://${ORIGIN}:8000/${pk}/${id}/message/`;
   const socket = new WebSocket(path);
    socket.onopen = e => {
      console.log("WebSocket open");

      
    };
    socket.onmessage = e => {
      console.log("Recieved ",e.data);
      let msgList = getState().chats.messages
      msgList.push(JSON.parse(e.data))
      dispatch({
        type:ADD_MESSAGE_TO_STATE,
        payload:msgList,
      })
      addMessageToState(msgList)
      // This should be added to the redux

    };

    socket.onerror = e => {
      console.log("Error", e);
    };

    socket.onclose = e => {
      console.log("closed", e);
      // this.getWebSocketConnection(id)
    };
    return socket
}

export const loadSocket = (socket) => (dispatch,getState) => {
  dispatch({
    type : ONOPEN,
    payload : socket
  })
}

export const sendSocketToState = (pk, socket, sockets) =>(dispatch) => {
  let obj = {
    secondUser: pk,
    socketobj: socket
  }
  sockets.push(obj)
  
  dispatch({
    type: ADD_SOCKET_TO_STATE,
    payload: sockets
  })
  
}

export const closeSocket = () => (dispatch) => {
  dispatch({
    type : ONCLOSE,
    payload : null
  })
}
export const onMessage = (messages) => (dispatch) => {
  dispatch({
    type:ONMESSAGE,
    payload:messages,
  })
}

export const addMessageToState = message => dispatch=> {
 
  dispatch({
    type:ADD_MESSAGE_TO_STATE,
    payload:message,
  })
}