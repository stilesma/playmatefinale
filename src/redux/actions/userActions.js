import {userConstant} from '../constants'
import axios from 'axios'
import {history} from '../../redux'

//SCROLL TO THE BOTTOM FOR MORE INFO


//http request to validate phone and receive otp
 const login = (phone_number) => dispatch =>{
     dispatch(login(phone_number))
    var data = {
        'phone_number' :   phone_number.code + phone_number.phone
    }

    const header = {
        'Content-Type' : 'application/json',
    }

    axios.post('http://192.168.104:8000/api/accounts/validate-phone/',data,{header:header})
     .then((response) => {
         console.log('confam',response)
     })
     
     .catch((err) => {
         console.log("error",err)
     })

     function login({phone_number}) {return {type:userConstant.LOGIN_REQUEST, phone_number}}
}
 //http request to verify otp, then push to register page

const authenticate = (phone_number,otp) => dispatch => {
    dispatch(authentiate_otp(phone_number,otp))
  var data = {
      'phone_number' : phone_number.code + phone_number.phone,
      'otp' : otp
  }

  const header = {
      'Content-Type': 'application/json'
  }
   console.log('registered')
  axios.post('http://192.168.43.104:8000/api/accounts/register/',data,{header:header})
  .then((response) => {
      console.log('confam',response)
      localStorage.setItem('token', response.data.token)

  })

  .then(phone_number => {
           history.push('./register$')
                }
   )
    function authentiate_otp(phone_number,otp) {return {type:userConstant.AUTHENTICATION
    , phone_number}}
}
//http request to add user_info to backend then push to image upload

const register = (user) => dispatch => {
    var data = {
      'user_info' : user 
    }

    const header = {
        'Content-Type' : 'application/json'
    }

    axios.put('http://192.168.1.8:8000/api/accounts/profile/2/update/',data,{header:header})
}

const add_image = (image,title,content) => dispatch => {
    dispatch(addImage(image,title,content))
    let form_data = new FormData();
    form_data.append('image', image,image.name)
    form_data.append('title', title)
    form_data.append('content', content)

    var data = form_data

    const header = {
        'Content-Type' : 'multipart/form-data'
    }

    axios.post('http://192.168.1.8:8000/api/account',data,header)
    .then((response) =>{
        console.log(response.data)
    })

 function addImage (image,title,content){return{type:userConstant.ADD_IMAGES,image,title,content}
} 
}

export const send_message =  (message) => dispatch => {
        dispatch(sendMessage(message))
    
        function sendMessage (message){return{
                                               type:userConstant.ADD_IMAGES,
                                               message
                                             }
}
}

export const set_message = (messages) => dispatch => {
    
        dispatch(setMessage(messages))

  function  setMessage(messages) {return {
                                            type:userConstant.SET_MESSAGES,
                                            messages:messages.reverse()
                                         }
}
}

export const render_message = (messages) => dispatch => {

      dispatch(renMessage(messages))
  
  function renMessage(messages) {return {
                                              type:userConstant.RENDER_MESSAGES,
                                               messages
                                           }

}
}
export const userActions = {
    login,
    authenticate,
    register,
    add_image,
    send_message,
    set_message,
    render_message,
}

//CONFIRM THE URL'S IF IT IS CORRECT
//TEST IF THE REDUCER CHANGES THE STATE AND  ADDS THE PHONE_NUMBER IN THE 'authenticate' function and
//USER IN THE 'register' FUNCTION 
//THE 'login' function accept a 'phone_number ' object consisting of 2 states;
//it sends a request then returns an otp

//HAPPY COMPILATION