import React, { Component } from 'react'
import AppBar from '@material-ui/core/AppBar'
import ChatIcon from '@material-ui/icons/Chat'
import PropTypes from 'prop-types'
import './sidebar.css'
import Friends from './Friends'
import {Typography,Tab,Tabs} from '@material-ui/core'

function TabContainer({ children, dir }) {
  return (
    <Typography component="div" style={{ padding: 0}}>
      {children}
    </Typography>
  );
}

TabContainer.propTypes = {
  children: PropTypes.node.isRequired,
  dir: PropTypes.string.isRequired,
};

 export class SideBar extends Component {
  constructor(props){
    super(props)
    this.state = {
      value: 0
    };
  }
    
      handleChange = (event, value) => {
        this.setState({ value });
      };
  render() {
      const {display_name,phone_number} = this.props
      const value = this.state
    return (
        <div className= "sam"
             style={{
                      width:450,
                      height:400,
                      backgroundColor:"transparent",
                      padding:0,
                      color:"#fffff"}}>

          <AppBar position="static" style={styles.appbar} color="inherit">
      <ChatIcon style={styles.chaticon}/>
        </AppBar>
        <Typography/>
        <div id="side-bar">
             <div className="users"
                  ref='users'>
                 <Friends display_name={display_name}/>
            </div>
            <div className="current-user">
             <span>{display_name}</span>
             <span>{phone_number}</span>
          </div>
        
          </div>
        
         
        </div>
    )
  }
} 
const styles = {
    appbar:{
        background:" linear-gradient(0.25turn,#f06292,#e91e63)",
        height:65
    },
    sidebar:{
      width:"450px",
      borderRadius:20,
    },
    tabs:{
      width:"450px",
      height:"100%",
      display:"flex",justifyContent:"space-around",
      flexDirection:"column",
      borderRadius:20,
    },
    chaticon:{
      width:70,
      height:70,
      marginLeft:"auto",
      marginRight:"auto"
    }
}

export default SideBar
