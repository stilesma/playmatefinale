import { userConstant } from '../constants'

export function chat (state={}, action){
    switch (action.type) {
        case userConstant.SEND_MESSAGES:
            return {
                ...state,
                messages:action.message
            }
        case userConstant.SET_MESSAGES:
            return {
                ...state,
                messages:action.messages
            }
        default:
            return state
    }
}