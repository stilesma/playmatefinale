import React, { Component } from 'react'
import {connect} from 'react-redux'


import ChatInline from './ChatInline'
import { CircularProgress } from '@material-ui/core';

export default class Chats extends Component {
   constructor(props){
       super(props)

   }

  render() {
      const {display_name} = this.props


    return (
      <div>
        
         {/*  {
            chatlist.length > 0 ? chatlist.map((chat, index)=>{
              return (
                <ChatInline key={index} info={chat.message} user_name={display_name}  />
              ) */
              <ChatInline/>
              }
            }) : 
            <div style={{
                       marginLeft:'auto',
                       marginRight:'auto',
                       width:'50%',
                       fontSize:'30px',
                       marginTop:'50%',
                      }}
            ><CircularProgress color= "secondary"/></div>
          }
      </div>
    )
  }
}