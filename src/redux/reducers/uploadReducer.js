import {userConstant} from "../constants";

const initialState = {
        title: '',
        content:'',
        image:null
}

export function upload( state= initialState, action) {
    switch (action.type) {
        case userConstant.ADD_IMAGES://ADD IMAGE TO STORE
            return state
        case userConstant.REMOVE_IMAGES:
            return state
        default:
            return state
    }
}