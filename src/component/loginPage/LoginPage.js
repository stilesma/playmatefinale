import React from 'react'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'
import Paper from '@material-ui/core/Paper'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import Dialog from '@material-ui/core/Dialog'
import DialogTitle from '@material-ui/core/DialogTitle'
import DialogContent from '@material-ui/core/DialogContent'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContentText from '@material-ui/core/DialogContentText'
import {COUNTRIES} from './CountryList'
import {userActions} from '../../redux/actions'
import {connect} from 'react-redux'

 class LoginPage extends React.Component {
  constructor(props){
    super(props);
    this.state={
      phone_number:{
        code:'+229',
        phone:'',
      },
      
      open: false,
      open1: false,
      otp:'',
      submitted:false
    }
  } 
    
  handleChange = name => e => {
    console.log(e.target.value)
    const {phone_number} = this.state
    this.setState({
      phone_number:{
                    ...phone_number,
                    [name]:e.target.value
                   }
    });
  };
 
  handleClickOpen = () => {
    const{phone_number} = this.state
  if(phone_number.phone.length>0){
    this.setState({ open: true })
  }

  };

  handleClose = () => {
    this.setState({ open: false });
  };
  handleClickOpen1 = (e) => {
    this.setState({ open1: true });
    this.setState({open: false})
    const {phone_number} = this.state;
    const {dispatch} = this.props
    if (phone_number) {
    dispatch(userActions.login(phone_number))
    }
  };

submitToken =(e) =>{
 /*  const {loggingIn} = this.props
  const {code,phone,otp,submitted} = this.state
  e.preventDefault();
  this.setState({loggingIn:true})
  this.setState({submitted:true});
  const {dispatch} = this.props
  if (code && phone){
      dispatch(userActions.login(code,phone))
  } */
 /*    var data = {
      'phone_number': code+phone,
      'otp': otp
    }
  const header ={
    'Content-Type': 'application/json',
    
}

  axios.post('http://192.168.43.104:8000/api/accounts/register/', data, {header:header})
    .then((response) =>{
      console.log("dff",response)
      localStorage.setItem('token',response.data.token)
      return(
        <Route path = "/register" component={RegisterPage}/>
      )
    })
    .catch((err) =>{
      console.log("errreerere",err)
    }) */
    const {phone_number,otp} = this.state
    const {dispatch} = this.props
    if (phone_number.code && phone_number.phone && otp){
      dispatch(userActions.authenticate(phone_number.code, phone_number.phone, otp))
    }
}

  handleClose1 = () => {
    this.setState({ open1: false });
  };
  render() {
     const {phone_number, otp} = this.state;
    // const {loggingIn} =this.props;
     
    return (
      <div>
          <AppBar position="static" style={styles.appbar}>
         <Toolbar>
             <Typography variant="h3" color="inherit" style={styles.typo1}>
            Playmate
             </Typography>
         
              </Toolbar>
         </AppBar>
            <div>
             <Paper style={styles.paper}>
             {/* <form className="Form"> */}
             <TextField
          id="standard-select-countryCode-native"
          select
        //  label="Country Code"
          className={styles.textField}
          value={phone_number.code}
        
          style={styles.textField1}
          onChange={this.handleChange('code')}
         SelectProps={{
            native: true,
            MenuProps: {
              className: styles.menu,
           },
         }}
        helperText="Please select your country" >
          {COUNTRIES.map(country => (
            <option key={country.twoLetter} value={country.code}>
              {country.country}
            </option>
          ))}
        </TextField>
        <br/>
        {/* <div className={`form-group ${this.errorClass(this.state.formErrors.phone)}`}> */}
        <TextField  
          type="mobileNo"
          label="Phone Number"
          //defaultValue=
          value={phone_number.phone}
          className="{textField}"
          helperText="Don't start with '0'"
          style={styles.textField2}
          onChange={this.handleChange('phone')}>
        </TextField>
        {/* </div> */}
        <br/>
        <Button style={styles.button}
        //onClick={alert(code+phone)} 
          type= "submit"
          onClick={this.handleClickOpen}
          disabled={phone_number.phone.length<0}
        >
          continue
        </Button>

        <Dialog
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-title">{"Validate"}</DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
             Do you want to continue with this number {this.state.code} {this.state.phone}
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color="primary">
              Back
            </Button>
            <Button onClick={this.handleClickOpen1} color="primary" autoFocus>
              Continue
            </Button>
          </DialogActions>
        </Dialog>
        <Dialog
          open={this.state.open1}
          onClose={this.handleClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-title">{"Confirmation"}</DialogTitle>
          <DialogContent>
          <TextField
          onChange={this.handleChange("otp")}
            value={otp}
             />
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose1} color="primary">
              Back
            </Button>
            <Button onClick={this.submitToken} color="primary" autoFocus>
              Continue
            </Button>
          </DialogActions>
        </Dialog>
            </Paper>
            </div>
            </div>
          )
        }
      }
      const styles = {
        paper:
        {
          backgroundColor:"#fafafa",
           width:300,
           height:400,
           marginLeft:"auto",
           marginRight:"auto",
           marginTop:100,
           textAlign:"center"
         
         },
         appbar:
         {
           color:"inherit",
           background:" linear-gradient(0.25turn, #e91e63, #f06292)" ,
           fontWeight: "bold",
           fontSize:30,
           fontStretch:10,
         },
         textField1:
         {
           marginTop:50,
         },
         textField2: 
         {
          color:"#DAA520",
          borderStyle:"solid",
          borderTopColor:"transparent",
          borderLeftColor:"transparent",
          borderRightColor:"transparent",
        },
         menu: 
         {
            border:1,
            width: 20,
            borderWidth: "1px",
            borderStyle:"solid",
            borderTopColor:"#212121",
            borderLeftColor:"#212121",
            borderRightColor:"#212121",
          borderBottomColor:"#DAA520",
          },
        button:
        {
          marginTop:30,
          background:" linear-gradient(0.25turn, #e91e63, #f69d3c)",
          borderRadius:20,
          color:"inherit",
          width:200,
          height:40,
          fontFamily: 'georgia',
          fontWeight: "bold",
          fontSize:20,
          padding:-10
        }
      }
      function mapStateToProps(state) {
        const { user } = state.user;
        return {
            user
        };
    }
    
    const connectedRegisterPage = connect(mapStateToProps)(LoginPage);
    export { connectedRegisterPage as LoginPage };