import React, { Component } from 'react'
import { ListItem,List,Divider,Paper,Typography, ListItemText, 
  ListItemAvatar,IconButton} from '@material-ui/core';


export default class ChatInline extends Component{

    render(){

        return(
            <div>
               <List>
                <ListItem alignItems='flex-start'>
                <Paper elevation ={4} style={styles.paper}>
                <ListItemAvatar>
                    <div></div>
            </ListItemAvatar>
                <ListItemText
          primary='Sylvanus Kizito'
          secondary={
            <React.Fragment>
            
              <div style={{display:'flex',
                        flexDirection:'row',
                        color:"#e91e63"
                       }}>
              <Typography
                component="span"
                variant="body2"
                color="inherit"
              >
                Dating
              </Typography>
              <Typography
                component="span"
                variant="body2"
                style={styles.mi}
                color="inherit"
              >
                2mi
              </Typography>
              
              </div>
              <Divider variant="middle"/>
              <div style={{display:'flex',
                        flexDirection:'row'
                       }}>
         <IconButton>
              Like
             </IconButton>
             <Divider style={styles.divide}/>
             <IconButton>
              No
             </IconButton>
             <Divider style={styles.divide}/>
             <IconButton>
             view more
             </IconButton>
             </div>

            </React.Fragment>
           
          }
        />
       
         
                </Paper>
                </ListItem>
                <Divider variant="middle" component="li" />
                
                </List>
            
            </div>
        )
    }
}

const styles = {
    paper:{
        width:400,
        marginLeft:'auto',
        marginRight:'auto',
    },

    heading: {
      fontSize: '15px',
      flexBasis: '33.33%',
      flexShrink: 0,
    },
    secondaryHeading: {
      fontSize: '15px',
      color: 'secondary',
    },

    divide: {
      width:4,
      height:60,
      color:'black'
    },
    mi:{
      float:'right' ,
      marginLeft:"auto"
    }
}