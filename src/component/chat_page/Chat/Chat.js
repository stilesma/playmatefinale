import React, { Component } from "react";
//import WebSocketInstance from "../WebSocket";
import Messages from "../Messages";
import MessageInput from "../MessageInput";
import { userActions } from "../../../redux/actions/userActions";
import './chat.scss'


export default class Chat extends Component {
  // constructor(props) {
  //   super(props);
  //   this.state = {};
  //
  //   // this.waitForSocketConnection(() => {
  //   //   WebSocketInstance.initChatUser(this.props.currentUser);
  //   //   WebSocketInstance.addCallbacks(
  //   //     this.setMessages.bind(this),
  //   //     this.addMessage.bind(this)
  //   //   );
  //   //   WebSocketInstance.fetchMessages(this.props.currentUser);
  //   // });
  // }

  state = {
    
  };

  componentDidMount() {
    // document.cookie = 'X-Authorization=' + authToken + ';';
    const location = window.location;
    let path = `ws://192.168.43.104:8000${location.pathname}`;
    // console.log(path);
    var socket = new WebSocket(path);
    socket.onopen = e => {
      console.log("WebSocket open", e);

      // Any time you sen a msg this is how you structure the object that will be sent to the layer
      var data = {
        message: "yo",
        phone_number: 13098765432,
        isTyping: false
      };
      // data = JSON.stringify(data);
      // console.log(data);
      socket.send(JSON.stringify(data));
    };
    socket.onmessage = e => {
      console.log(e.data);
      //THis is where you publish the message coming from the back end
      // socket.send(JSON.stringify(e.data));
      // socketNewMessage(e.data);
    };

    socket.onerror = e => {
      console.log("Error", e);
    };

    socket.onclose = e => {
      console.log("closed", e);
    };
  }

  waitForSocketConnection(callback) {
    //const component = this;

    // setTimeout(
    //     function () {
    //         if (WebSocketInstance.state()===1) {
    //             console.log ("Connection is made")
    //             callback();
    //             return;
    //         } else {
    //             console.log("wait for cnneection...")
    //             component.waitForSocketConnection(callback);
    //         }
    //     }
    // ,100)
  }

  addMessage(message) {
    const {dispatch} = this.props
    if (message){
    dispatch(userActions.send_message(message))
    }
   // this.setState({ messages: [...this.state.messages, message] });
  }

  setMessages(messages) {
    const {dispatch} = this.props
    if(messages){
      dispatch(userActions.set_message(messages))
    }
    //this.setState({ messages: messages.reverse() });
  }

  messageChangeHandler = event => {
    event.preventDefault();
    this.setState({
      message: event.target.value
    });
  };

  // sendMessageHandler = (e, message) => {
  //   const messageObject = {
  //     from: this.props.currentUser,
  //     text: message
  //   };
  //   WebSocketInstance.newChatMessage(messageObject);
  //   this.setState({
  //     message: ""
  //   });
  //   // e.preventDefault();
  // };

  renderMessages = (messages) => {
    console.log(messages)
    const currentUser = 'stiles';
   // const {dispatch} = this.props

    return messages.map((message, i) => (
      <li
        key={message.id}
        className={message.author === currentUser ? "me" : "him"}
      >
        <h4 className="author">{message.author}</h4>
        <p>{message.content}</p>
      </li>
    
    ) 
    ); 
  
  };
  render() {
    const messages = this.state.messages;
    return (
      <div>
        <Messages messages={messages} renderMessages={this.renderMessages} />
        <MessageInput
          message={this.state.message}
          messages={messages}
          renderMessages={this.renderMessages}
          messageHandler={this.messageChangeHandler}
        />
      </div>
    );
  }
}
