import React, { Component } from 'react'

class TestLogin extends Component {
   state = {
     phone: ''
   }

   onSubmit = e => {
     e.preventDefault()
   }

   handleChange = e => {
    this.setState({[e.target.name]: e.target.value})
   }

   render() {
     const {phone} = this.state
     return (
       <div>
         <h1>Enter your Phone for Validation</h1>
         <form>
         <label>Phone</label>
            <input
              type="tel"
              name="phone"
              onChange={this.handleChange}
              value={phone}
              />
              <button type="submit" className="btn btn-primary">
              Submit
            </button>
         </form>
       </div>
     )
   }
 }
 

//  const mapStateToProps = state => ({
//   isAuthenticated: state.auth.isAuthenticated
// })
// export default connect(mapStateToProps, {TestLogin})(TestLogin)