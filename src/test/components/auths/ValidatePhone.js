import React, { Component } from 'react'
import {connect} from 'react-redux'
import {Redirect} from "react-router-dom"
import PropTypes from 'prop-types'
import PhoneInput from 'react-phone-number-input'
import 'react-phone-number-input/style.css'

import {validatePhone} from '../../actions/auths'

import Paper from '@material-ui/core/Paper'
import Button from '@material-ui/core/Button'
import CheckBox from '@material-ui/core/Checkbox'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import './phone.css'

class ValidatePhone extends Component {

  static propTypes = {
    validatePhone: PropTypes.func.isRequired,
    isAuthenticated: PropTypes.bool,
    login:PropTypes.bool,
    register: PropTypes.bool
  }

  state = {
    phone_number: null,
    registerAction: false,
  }

  handleSubmit = e => {
    e.preventDefault()

    let payload = {"phone_number": this.state.phone_number}
    if(this.state.registerAction){
      payload['register'] = true
      payload['login'] = false
    }
    else if(!this.state.registerAction){
      payload['register'] = false
      payload['login'] = true
    }
    
    
    this.props.validatePhone(payload);
    
  }


  handleRegister = e => {
    const registerAction = e.target.checked;
    this.setState({registerAction})

  }


  render() {
      if(this.props.isAuthenticated){
      return <Redirect to="/" />
    }
    if(this.props.register){
      return <Redirect to="/register" />
    }
    if(this.props.login){
      return <Redirect to="/login" />
    } 
     
    const {phone_number, registerAction} = this.state
    return (
      <div>
        <AppBar position="static" style={styles.appbar}>
            <Toolbar>
                <Typography variant="h3" color="inherit" style={styles.typo}>
                  Playmate
                </Typography>
            </Toolbar>
        </AppBar>
        <Paper style={styles.paper}>
          <form onSubmit={this.handleSubmit}>

            <PhoneInput
                className='phone'
                style={styles.phone}
                placeholder="Enter phone number"
                value={ phone_number }
                onChange={ phone_number => this.setState({ phone_number }) } />

            <label htmlFor="register" style={{color:"#e91e63"}}>Register </label>
            <CheckBox 
            checked={registerAction}
            onChange={this.handleRegister}
              type="checkbox"
              name="register"/>

            <Button  type="submit" style={styles.button}>Submit</Button>

          </form>
        </Paper>
      </div>
    )
  }
}
const styles = {
    phone:{
      position:'relative',
      width:260,
      backgroundColor:'none',
      display:'inline-block',
      marginTop: 50,
    },
    appbar:
    {
      color:"inherit",
      background:" linear-gradient(0.25turn, #e91e63, #f06292)" ,
      fontWeight: "bold",
      fontSize:30,
      fontStretch:10,
    },
    paper:{
      width: 300,
      height: 300,
      marginLeft: 'auto',
      marginRight: 'auto',
      textAlign: 'center',
      marginTop: '13%',
     
    },
    button:{
      marginTop:30,
      background:" linear-gradient(0.25turn, #e91e63, #f69d3c)",
      borderRadius:20,
      color:"inherit",
      width:200,
      height:40,
      fontFamily: 'georgia',
      fontWeight: "bold",
      fontSize:20,
      padding:-10
    },
    typo: {
      marginLeft:'auto',
      marginRight:'auto',
    }
}

const mapStateToProps = state => ({
  isAuthenticated: state.auths.isAuthenticated,
  login: state.auths.login,
  register: state.auths.register,
})
export default connect(mapStateToProps, {validatePhone})(ValidatePhone)