
import {
  THREAD_LOADING_FAIL,
  THREAD_LOADING,
  THREAD_LOADED,

  CHATLIST_LOADED,
  CHATLIST_LOADING,
  CHATLIST_LOADING_FAIL, 

  CHAT_LOADED,
  CHAT_LOADING,
  CHAT_LOADING_FAIL,

  MESSAGE_LOADED,
  MESSAGE_LOADING,
  MESSAGE_LOADING_FAIL,

  SECOND_USER_LOADED,
  SECOND_USER_LOADING,
  SECOND_USER_LOADING_FAIL,
  
  SOCKET,
  ONMESSAGE,
  ONCLOSE,
  ONOPEN,
  ADD_SOCKET_TO_STATE,
  LOGOUT_SUCCESS,
  AUTH_ERROR,

  
} from '../actions/types'

const initialState = {
  isLoading: false,
  thread: {},
  chatlist: [],
  chatMessages: [],
  secondUser:{},
  messages:[],
  socket:null,
  received:[],
  sockets:[]
}

export default function(state=initialState, action){
  switch (action.type) {
    case THREAD_LOADED:
      return {
        ...state,
        isLoading: false,
        thread: action.payload
      }
    case CHATLIST_LOADED:
      return {
        ...state,
        isLoading: false,
        chatlist: action.payload
      }
    case MESSAGE_LOADED:
      return {
        ...state,
        isLoading: false,
        messages: action.payload
      }
    case CHAT_LOADED:
      return {
        ...state,
        chatMessages: action.payload
      }
    case SECOND_USER_LOADED:
        return {
          ...state,
          secondUser: action.payload
        }
    case ADD_SOCKET_TO_STATE:
      return {
        ...state,
        sockets: action.payload
      }
    
    case SECOND_USER_LOADING:
    case MESSAGE_LOADING:
    case CHATLIST_LOADING:
    case THREAD_LOADING:
    case CHAT_LOADING:
      return {
        ...state,
        isLoading: true
      }
    
    case CHAT_LOADING_FAIL:
      return {
        ...state,
        isLoading: false,
        chatMessages: []
      }
    case MESSAGE_LOADING_FAIL:
        return {
          ...state,
          isLoading: false,
          messages: []
        }
    case THREAD_LOADING_FAIL:
        return {
          ...state,
          isLoading: false,
          thread:''
        }
      case SECOND_USER_LOADING_FAIL:
      return {
        ...state,
        isLoading: false,
        thread:null
      }
    case CHATLIST_LOADING_FAIL:
      return {
        ...state,
        isLoading: false,
        chatlist: [],
      }
    case AUTH_ERROR:
    case LOGOUT_SUCCESS:
      return {
        isLoading: false,
        thread: {},
        chatlist: [],
        chatMessages: [],
        secondUser:{},
        messages:[],
        socket:null,
        received:[],
        sockets:[]
      }
    case ONMESSAGE:
      return {
        ...state,
        received:action.payload
      }
    case ONOPEN: 
      return {
        ...state,
        socket:action.payload
      }
    case ONCLOSE:
      return {
        ...state,
        socket:null
      }
    default:
      return state;
  }
}