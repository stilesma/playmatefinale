import React from 'react'
import Tabs from '@material-ui/core/Tabs'
import Tab from '@material-ui/core/Tab'
import Typography from '@material-ui/core/Typography'
import TextField from '@material-ui/core/TextField'
import AppBar from '@material-ui/core/AppBar'
import Button from '@material-ui/core/Button'
import Toolbar from '@material-ui/core/Toolbar'
import PropTypes from 'prop-types'
import MenuItem from '@material-ui/core/MenuItem'
import Divider from '@material-ui/core/Divider'
import InputAdornment from '@material-ui/core/InputAdornment'
import Switch from '@material-ui/core/Switch'
import Slider from '@material-ui/lab/Slider'
import LensIcon from '@material-ui/icons/LensOutlined';
import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListSubheader from '@material-ui/core/ListSubheader'
import List from '@material-ui/core/List'
import ListItemText from '@material-ui/core/ListItemText'
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction'
import PetsIcon from '@material-ui/icons/Pets'
import DirectionsRunIcon from '@material-ui/icons/DirectionsRun'
import MusicNoteIcon from '@material-ui/icons/MusicNote'
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos'
import {connect} from 'react-redux';
import {userActions} from '../../redux/actions';
import './editr.css';
import {GENDER,GendeR,HEIGHT,AGE,ETHNICITY,BODYTYPE} from './List'


function TabContainer(props) {
  return (
    <Typography component="div" style={{ padding: 8 * 3 }}>
      {props.children}
    </Typography>
  );
}

TabContainer.propTypes = {
  children: PropTypes.node.isRequired,
};

 class RegisterPage extends React.Component {
  constructor(props){
    super(props)
  this.state = {
     value: 0, 
     submitted:false,
     user:
     {
      slideValue:10,
      username:'',
      error:'',
      firstName:'',
      lastName:'',
      description:'',
      aGe:'18',
      ethniCity:'American Indian or Alaska Native',
      heiGht:'160',
      gender:'male',
      genDER:'female',
      bodyType:'slim',
      diet:false,
      drink:false,
      drugs:false,
      smoke:false,
      philosophy:false, 
      photography:false,            
      soccer:false,
      pop:false,          
      rock_n_Roll:false,
      afroBeats:false,
      indoorgames:false,    
      hookup:false,
      dating:false,                    
      swimming:false,
      gymnastics:false,
      dogs:false,  
      cats:false,
      birds:false,
      rabbits:false,                       
      movies:false,
      sightSeeing:false,              
      athletics:false,
      health:false,                       
      technology:false,
     },
      response:{}
  };

}


  handleChange = (event, value) => {
    this.setState({ value });
  };

  handleChanged = name => e => {
    console.log(e.target.value)
    const {user} = this.state;
    this.setState({
        user:{
               ...user,
               [name]:e.target.value
             }
    });
  };
  
  handleCheck = name=>  e => {
  
    const {user} = this.state;
    this.setState({
        user:{
               ...user,
               [name]:e.target.checked
             }
    });
  };

  handleSelect = prop =>  e => {
    const {user} = this.state;
    this.setState({
        user:{
               ...user,
               [prop]:e.target.value
             }
    });
  };

  handleSlide = (event, slideValue) => {
    const {user} = this.state;
    this.setState({
        user:{
               ...user,
               slideValue
             }
    });
  };

  setError = (error) =>{
    this.setState({error})
}

handleSubmit = (event) => {
  event.preventDefault();
  this.setState({submitted:true});
  const {user} = this.state;
  const {dispatch} = this.props;
  if (user.firstName && user.lastName && user.username){
    dispatch(userActions.register(user))
  }
}

randomPlaceholder(){
  const rand = ["Ricky32","trille","SexyCat","BadGuy","Stiles349","Dany","Minipapa"]
  return rand[Math.floor(Math.random()*3000)%rand.length]
}

  render() {

     const {user,value} = this.state
    
return (
        <div>
            <AppBar position="static" style={styles.appbar}>
         <Toolbar>
             <Typography variant="title" color="inherit" style={styles.typo1}>
             EDIT PROFILE
             </Typography>
             <Button color="inherit"
              type="submit"
              onClick={this.handleSubmit}
              style={styles.button}
              ><ArrowForwardIosIcon/></Button>
              </Toolbar>
         </AppBar> 
              <div className="edit">
                 
       
   <Tabs
          value={value}
          onChange={this.handleChange}
>
          <Tab
            disableRipple
            label="Edit Profile"
          />
          <Tab
            disableRipple
            label="Preference"
          />
          <Tab
            disableRipple
            label="Interest"
          />
        </Tabs>
        {value === 0 && 
        <TabContainer>
          <div style={{width:680, marginLeft:"auto", marginRight:"auto"}}>
         <TextField 
                style={styles.username}
                placeholder="First Name"
                label="First Name"
                value={user.firstName}
                onChange={this.handleChanged("firstName")}/>
            <br/>
            <Divider variant="middle" />
           <TextField style={styles.lastname} 
                       placeholder="Last Name"
                       label="Last Name"
                       value={user.lastName}
                       onChange={this.handleChanged("lastName")}
             />
            <br/>
            <Divider variant="middle" />
            <TextField style={styles.lastname} 
                       placeholder={this.randomPlaceholder()}
                       label="Username"
                       value={user.username}
                       onChange={this.handleChanged("username")}

             />
              <div>{user.error ? user.error : null}</div>
                   <Divider variant="middle" />
                   <List subheader={<ListSubheader>Personal Profile</ListSubheader>} >
            <ListItem>
            <ListItemText primary="Age"/>
            <ListItemSecondaryAction>
            <TextField
            select
            value={user.aGe}
            onChange={this.handleSelect('aGe')}
            style={styles.select}
            InputProps={{
              endAdornment:<InputAdornment position="end">yrs</InputAdornment>}}
            >
            {AGE.map(option => (
            <MenuItem key={option.value} value={option.value}>
              {option.label}
            </MenuItem>
          ))}
            </TextField>
            </ListItemSecondaryAction>
            </ListItem>
                   <Divider variant="middle" />
            <ListItem>
            <ListItemText primary="Ethnicity"/>
            <ListItemSecondaryAction>
            <TextField
            select
            value={user.ethniCity}
            onChange={this.handleSelect('ethniCity')}
            style={styles.select1}
           >
            {ETHNICITY.map(option => (
            <MenuItem key={option.value} value={option.value}>
              {option.label}
            </MenuItem>
          ))}
           </TextField>
            </ListItemSecondaryAction>
            </ListItem>
            <Divider variant="middle" />
            <ListItem>
            <ListItemText primary="Height"/>
            <ListItemSecondaryAction>
            <TextField
            select
            value={user.heiGht}
            onChange={this.handleSelect('heiGht')}
            style={styles.select1}
            InputProps={{
              endAdornment:<InputAdornment position="end">cm</InputAdornment>}}
            >
           >
            {HEIGHT.map(option => (
            <MenuItem key={option.value} value={option.value}>
              {option.label}
            </MenuItem>
          ))}
           </TextField>
            </ListItemSecondaryAction>
            </ListItem>
            <Divider variant="middle" />
            <ListItem>
            <ListItemText primary="Body Type"/>
            <ListItemSecondaryAction>
            <TextField
            select
            value={user.bodyType}
            onChange={this.handleSelect('bodyType')}
            style={styles.select1}
           >{BODYTYPE.map(option => (
              <MenuItem key={option.value} value={option.value}>
                {option.label}
              </MenuItem>
          ))} 

            </TextField>
            </ListItemSecondaryAction>
            </ListItem>
            <Divider variant="middle" />
            <ListItem>
            <ListItemText primary="Gender"/>
            <ListItemSecondaryAction>
            <TextField
            select
            value={user.gender}
            onChange={this.handleSelect('gender')}
            style={styles.select1}
           > {GENDER.map(option => (
            <MenuItem key={option.value} value={option.value}>
              {option.label}
            </MenuItem>
          ))} </TextField>     
          
            </ListItemSecondaryAction>
            </ListItem>
            <Divider variant="inset" /> 
            <br/>
       </List>

        <TextField
               id="filled-multiline-static"
               label="Description"
               placeholder="Write about yourself"
               multiline
               rows="4"
               value={user.description}
               margin="normal"
               variant="outlined"
               style={styles.desc}
               onChange={this.handleChanged('description')}
               
                />
       </div>
        </TabContainer>}
        {value === 1 && 
        <TabContainer> 
          <div style={{width:680, marginLeft:"auto", marginRight:"auto"}}>
          <List subheader={<ListSubheader>Settings</ListSubheader>} >
          <Divider component="li" variant="inset" />
          <ListItem>
            <ListItemText primary="Diet" style={styles.li}/>
            <ListItemSecondaryAction>
            <Switch
              checked={user.diet}
              onChange={this.handleCheck('diet')}
              value="diet"
              color="secondary"
            />
            </ListItemSecondaryAction>
            </ListItem>
            <Divider component="li" variant="inset" />
            <ListItem>
            <ListItemText primary="Smoke" style={styles.li}/>
            <ListItemSecondaryAction>
            <Switch
              checked={user.smoke}
              onChange={this.handleCheck('smoke')}
              value="smoke"
              color="secondary"
            />
            </ListItemSecondaryAction>
          </ListItem>
          <Divider component="li" variant="inset" />
          <ListItem>
            <ListItemText primary="Drink" style={styles.li}/>
            <ListItemSecondaryAction>
            <Switch
              checked={user.drink}
              onChange={this.handleCheck('drink')}
              value="drink"
              color="secondary"
            />
            </ListItemSecondaryAction>
            </ListItem>
            <Divider component="li" variant="inset" />
            <ListItem>
            <ListItemText primary="Drugs" style={styles.li}/>
            <ListItemSecondaryAction>
            <Switch
              checked={user.drugs}
              onChange={this.handleCheck('drugs')}
              value="drugs"
              color="secondary"
            />
            </ListItemSecondaryAction>
            </ListItem>
      </List>
         <Divider variant="fullWidth" />
         <br/>
         <List subheader="Discovery Setting">
         <Divider component="li" variant="inset" />
           <ListItem>
             <ListItemText primary="Maximum Distance of Chat"style={styles.li}/>
             <ListItemText primary={user.slideValue+'mi'}/>
             <ListItemSecondaryAction>
              <Slider
               value={user.slideValue}
               min={1}
               max={100}
               step={1}
               aria-labelledby="slider-icon"
              onChange={this.handleSlide}
               style={styles.slide}
               thumb={<LensIcon style={{ color: '#2196f3' }} />}
        /> 
             </ListItemSecondaryAction>
             </ListItem>
             <Divider component="li" variant="inset" />
             <ListItem>
               <ListItemText primary="Looking for" style={styles.li}/>
               <ListItemSecondaryAction>
                <TextField
                 select
                 value={user.genDER}
                 onChange={this.handleSelect('genDER')}
                 style={styles.selectSex}>
                  {GendeR.map(option => (
                   <MenuItem key={option.value} value={option.value}>
                    {option.label}
                   </MenuItem>
                  ))}
                </TextField>
               </ListItemSecondaryAction>
             </ListItem>
             <Divider component="li" variant="inset" />
              <ListItem>
               <ListItemText primary="City"/>
               <ListItemSecondaryAction>
                 <TextField
                   style={styles.username}/>
               </ListItemSecondaryAction>
             </ListItem> 
         </List>
         <List subheader="Status">
         <Divider component="li" variant="inset" />
          <ListItem>
          <ListItemText primary="Hookup/Escort" style={styles.li}/>
            <Switch
              checked={user.hookup}
              onChange={this.handleCheck('hookup')}
              style={styles.interes}
              value="hookup"
              color="secondary"
            />
          </ListItem>
          <Divider component="li" variant="inset" />
          <ListItem>
          <ListItemText primary="Dating" style={styles.li}/>
            <Switch
              checked={user.dating}
              onChange={this.handleCheck('dating')}
              style={styles.interes}
              value="dating"
              color="secondary"
            />
          </ListItem>
          <Divider component="li" variant="inset" />
         </List>
        </div >
        </TabContainer>}
        {value === 2 && 
        <TabContainer> 
           <div style={{width:680, marginLeft:"auto", marginRight:"auto"}}>
           <List subheader="Animals">
           <ListItemIcon>
                <PetsIcon/>
           </ListItemIcon>
           <Divider component="li" variant="inset" />
           <ListItem>
             <ListItemText primary="Dogs" style={styles.li}/>
            <Switch
              checked={user.dogs}
              onChange={this.handleCheck('dogs')}
              style={styles.interes}
              value="dogs"
              color="secondary"
            />
            </ListItem>
            <Divider component="li" variant="inset" />
            <ListItem>
             <ListItemText primary="Cats" style={styles.li}/>
            <Switch
              checked={user.cats}
              onChange={this.handleCheck('cats')}
              style={styles.interes}
              value="cats"
              color="secondary"
            />
            </ListItem>
            <Divider component="li" variant="inset" />
            <ListItem>
             <ListItemText primary="Rabbits" style={styles.li}/>
            <Switch
              checked={user.rabbits}
              onChange={this.handleCheck('rabbits')}
              style={styles.interes}
              value="rabbits"
              color="secondary"
            />
            </ListItem>
            <Divider component="li" variant="inset" />
            <ListItem>
             <ListItemText primary="Birds" style={styles.li}/>
            <Switch
              checked={user.birds}
              onChange={this.handleCheck('birds')}
              style={styles.interes}
              value="birds"
              color="secondary"
            />
            </ListItem>
            </List>
            <Divider variant="fullWidth"/>
            <List subheader="Sports">
           <ListItemIcon>
                <DirectionsRunIcon/>
           </ListItemIcon>
           <Divider component="li" variant="inset" />
            <ListItem>
            <ListItemText primary="Athletics" style={styles.li}/>
            <Switch
              checked={user.athletics}
              onChange={this.handleCheck('athletics')}
              style={styles.interes}
              value="athletics"
              color="secondary"
            />
            </ListItem>
            <Divider component="li" variant="inset" />
            <ListItem>
            <ListItemText primary="Indoor Games" style={styles.li}/>
            <Switch
              checked={user.indoorgames}
              onChange={this.handleCheck('indoorgames')}
              style={styles.interes}
              value="indoorgames"
              color="secondary"
            />
            </ListItem>
            <Divider component="li" variant="inset" />
            <ListItem>
       <ListItemText primary="Gymnastics" style={styles.li}/>
            <Switch
              checked={user.gymnastics}
              onChange={this.handleCheck('gymnastics')}
              style={styles.interes}
              value="gymnastics"
              color="secondary"
            />
           </ListItem>
           <Divider component="li" variant="inset" />
          <ListItem>
          <ListItemText primary="Soccer" style={styles.li}/>
            <Switch
              checked={user.soccer}
              onChange={this.handleCheck('soccer')}
              style={styles.interes}
              value="soccer"
              color="secondary"
            />
            </ListItem>
            <Divider component="li" variant="inset" />
          <ListItem>
          <ListItemText primary="Swimming" style={styles.li}/>
            <Switch
              checked={user.swimming}
              onChange={this.handleCheck('swimming')}
              style={styles.interes}
              value="swimming"
              color="secondary"
            />
            </ListItem>
            </List>
            <Divider variant="fullWidth"/>
            <List subheader="Music">
              <ListItemIcon>
                <MusicNoteIcon/>
              </ListItemIcon>
              <Divider component="li" variant="inset" />
            <ListItem>
          <ListItemText primary="Pop" style={styles.li}/>
            <Switch
              checked={user.pop}
              onChange={this.handleCheck('pop')}
              style={styles.interes}
              value="pop"
              color="secondary"
            />
            </ListItem>
            <Divider component="li" variant="inset" />
            <ListItem>
            <ListItemText primary="Rock_n_Roll" style={styles.li}/>
            <Switch
              checked={user.rock_n_Roll}
              onChange={this.handleCheck('rock_n_Roll')}
              style={styles.interes}
              value="rock_n_Roll"
              color="secondary"
            />  
            </ListItem>
            <Divider component="li" variant="inset" />
            <ListItem>
            <ListItemText primary="Afro Beats" style={styles.li}/>
            <Switch
              checked={user.afroBeats}
              onChange={this.handleCheck('afroBeats')}
              style={styles.interes}
              value="afroBeats"
              color="secondary"
            />  
            </ListItem>
            </List>
             <Divider variant="fullWidth"/>
            <List subheader="Others">
            <Divider component="li" variant="inset" />
            <ListItem>
            <ListItemText primary="Health" style={styles.li}/>
            <Switch
              checked={user.health}
              onChange={this.handleCheck('health')}
              style={styles.interes}
              value="health"
              color="secondary"
            />
            </ListItem>
            <Divider component="li" variant="inset" />
        <ListItem>
        <ListItemText primary="Philosophy" style={styles.li}/>
            <Switch
              checked={user.philosophy}
              onChange={this.handleCheck('philosophy')}
              style={styles.interes}
              value="philosophy"
              color="secondary"
            />
             </ListItem>
             <Divider component="li" variant="inset" />
         
      
          <ListItem>
          <ListItemText primary="Sight Seeing" style={styles.li}/>
            <Switch
              checked={user.sightSeeing}
              onChange={this.handleCheck('sightSeeing')}
              style={styles.interes}
              value="sightSeeing"
              color="secondary"
            />
            </ListItem>
         <Divider component="li" variant="inset" />
         <ListItem>
         <ListItemText primary="Movies" style={styles.li}/>
            <Switch
              checked={user.movies}
              onChange={this.handleCheck('movies')}
              style={styles.interes}
              value="movies"
              color="secondary"
            />
            </ListItem>
            <Divider component="li" variant="inset" />
       <ListItem>
       <ListItemText primary="Technology" style={styles.li}/>
            <Switch
              checked={user.technology}
              onChange={this.handleCheck('technology')}
              style={styles.interes}
              value="technology "
              color="secondary"
            />
            </ListItem>
            <Divider component="li" variant="inset" />
       <ListItem>
       <ListItemText primary="Photography" style={styles.li}/>
            <Switch
              checked={user.photography}
              onChange={this.handleCheck('photography')}
              style={styles.interes}
              value="photography "
              color="secondary"
            />
            </ListItem>
         </List>
           </div>
         </TabContainer>}
         </div>
         </div>
    )
   
  }
}
const styles={
  avatar:{
    position:"relative",
    marginLeft:"auto",
    marginRight:"auto",
    width:200,
    height:200,
  },
  appbar:{
    color:"inherit",
    background:" linear-gradient(0.25turn, #e91e63, #f06292)",
    fontWeight: "bold",
   
},
typo1:{
  marginLeft:"auto",
  fontFamily: 'yatra one regular',
  fontWeight: "bold",
  fontSize:39,
},
button:{
  marginLeft:"auto",
  fontFamily: 'yatra one regular',
  fontWeight: "bold",
  fontSize:24,
},
username:{
width:300,

},
lastname:{
  width:300,
  
},
slide:{
  width:120,
  //height:20
},
select:{
  width:190,
  margin:10,
  
  },
  select1:{
    width:100,
    margin:10
    
    },
    selectSex:{
      width:150,
      margin:10
      
      },
    li:{
    marginLeft: 55
    },
    desc:{
      width:400
    }
}

function mapStateToProps(state){
  const {registering} = state.registration;
  return{
    registering
  };
}

const connectedRegisterPage = connect(mapStateToProps)(RegisterPage);
export {connectedRegisterPage as RegisterPage};
