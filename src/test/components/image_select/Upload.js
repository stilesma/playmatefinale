import React, {Component} from 'react'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import { Button } from '@material-ui/core';
import ArrowBackIosRoundedIcon from '@material-ui/icons/ArrowBackIosRounded'
import jerry from './jerry.jpg'
import {addImage} from '../../actions/auths'
import {connect} from  'react-redux'

class Upload extends Component {
    constructor(props){
        super(props)
        this.state = {
            /* title: '',
            content:'',
            image:[],
            image:[] */
            file:'',
            imagePreviewUrl:''
        };
    }
       /*  handleChange = (e) => {
            this.setState({
                [e.target.id]: e.target.value
            })
        };

        handleImageChange = (e) => {
            this.setState({
                image: e.target.files[0]
            })
        };

        handleSubmit = (e) => {
   
            } */
       
      handleSubmit = (e) => {
          e.preventDefault();
          console.log('handle uploading-', this.state.file);
      }

      handleImageChange = (e) => {
        e.preventDefault();
        
        let reader = new FileReader();
        let file = e.target.files[0];

        reader.onloadend = () => {
            this.setState({
                file: file,
                imagePreviewUrl:reader.result})
            
        }
        reader.readAsDataURL(file)
      }
    
    render() {
        let {imagePreviewUrl} = this.state
        let imagePreview = null
        if(imagePreviewUrl){
            imagePreview = (<img src={imagePreviewUrl}/>)
        }
        //const {image} = this.state
        return (
            <div>
              {/* <form onSubmit={this.handleSubmit}>
                <h2>dsff</h2>
              </form>
              <div>
                  {
                      image.length>1
                      ?
                      null 
                      :
                   <div style={{display:'flex'}}>
                          <div className="present"
                               style=
                                 {{
                                    width:400,
                                    height:400,
                                    backgroundColor:'#fafafa',
                                    marginRight:'auto',
                                    marginLeft:'auto',
                                    display:'flex',
                                    position:'relative'
                                 }}
                          >
                              
                          <Button/>
                      <Button
                          style={styles.button}/>
                      
                          </div>
                   </div>
                  }

              </div> */}
               <form onSubmit={this._handleSubmit}>
          <input type="file" onChange={this._handleImageChange} />
          <button type="submit" onClick={this._handleSubmit}>Upload Image</button>
        </form>
        {imagePreview}
      </div>

        )
    }
}

const styles = {
    appbar:
         {
           color:"inherit",
           background:" linear-gradient(0.25turn, #e91e63, #f06292)" ,
           fontWeight: "bold",
           fontSize:30,
           fontStretch:10,
          
         },
    button:
         {
            marginLeft:'auto'
         }
}
const mapStateToProps = state => ({
    
})
export default connect(mapStateToProps, {addImage})(Upload)