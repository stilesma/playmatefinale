import React, {Component} from 'react'
import Typography from '@material-ui/core/Typography'
import TextField from '@material-ui/core/TextField'
import AppBar from '@material-ui/core/AppBar'
import Button from '@material-ui/core/Button'
import Toolbar from '@material-ui/core/Toolbar'
import PropTypes from 'prop-types'
import MenuItem from '@material-ui/core/MenuItem'
import Divider from '@material-ui/core/Divider'
import InputAdornment from '@material-ui/core/InputAdornment'
import Switch from '@material-ui/core/Switch'
import Slider from '@material-ui/lab/Slider'
import LensIcon from '@material-ui/icons/LensOutlined';
import ListItem from '@material-ui/core/ListItem'
import ListSubheader from '@material-ui/core/ListSubheader'
import List from '@material-ui/core/List'
import ListItemText from '@material-ui/core/ListItemText'
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction'
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos'
import {connect} from 'react-redux';
import {addUserInfo} from '../../actions/auths'
//import './editr.css';
import {GENDER,HEIGHT,AGE,BODYTYPE, CHAT_GENDER} from './List'
import {Paper,Grid} from '@material-ui/core'
import {Redirect} from "react-router-dom"


function TabContainer(props) {
  return (
    <Typography component="div" style={{ padding: 8 * 3 }}>
      {props.children}
    </Typography>
  );
}

TabContainer.propTypes = {
  children: PropTypes.node.isRequired,
};


 class UserInfo extends Component {
   static propTypes = {
     userDetails:PropTypes.object
   }
  constructor(props){
    super(props)
  this.state = {
     value: 0, 
     userDetails:
     {
       display_name:'',
       first_name:'',
       last_name:'',
       email: '',
       description:'',
       age:'18',
       height:'160',
       gender:'male',
       body_type:'slim',
       chat_distance:10,
       chat_gender:'female',
       hookup:false,
       dating:false
     },
  
  };

}


  handleChange = (event, value) => {
    this.setState({ value });
  };

  handleChanged = name => e => {
    const {userDetails} = this.state;
    this.setState({
        userDetails:{
               ...userDetails,
               [name]:e.target.value
             }
    });
  };
  
  handleCheck = name =>  e => {
      const {userDetails} = this.state;
      this.setState({
        userDetails:{
               ...userDetails,
               [name]:e.target.checked
                }
    });
  };

  handleSelect = prop =>  e => {
    const {userDetails} = this.state;
    this.setState({
        userDetails:{
               ...userDetails,
               [prop]:e.target.value
             }
    });
  };

  handleSlide = (event, chat_distance) => {
   
    const {userDetails} = this.state;
    this.setState({
        userDetails:{
               ...userDetails,
               chat_distance
             }
    });
  };

  setError = (error) =>{
    this.setState({error})
}

handleSubmit = (e) => {
  e.preventDefault();
  this.props.addUserInfo(JSON.stringify(this.state.userDetails))
}

randomPlaceholder(){
  const rand = ["Ricky32","trille","SexyCat","BadGuy","Stiles349","Dany","Minipapa"]
  return rand[Math.floor(Math.random()*3000)%rand.length]
}

  render() {
    /* if(this.props.register === false){
      return(
        <Redirect to="/" />
      )
    } */
     const {userDetails,value} = this.state
    
return (
        <div>
          <Grid container>
          <Grid item xs={12}>
          <AppBar position="static" style={styles.appbar}>
              <Toolbar>
                  <Typography variant="title" color="inherit" style={styles.typo1}>
                       EDIT PROFILE
                  </Typography>
                  <Button color="inherit"
                          type="submit"
                          style={styles.button}>
                            <ArrowForwardIosIcon/>
                  </Button>
              </Toolbar>
           </AppBar> 
              <div className="edit">
              <form onSubmit={this.handleSubmit}>
                 <div style={{position:'relative'}}>
                    <List color="inherit" subheader={<ListSubheader>Personal Profile</ListSubheader>} >
                  
                           <TextField style={styles.username}
                                      placeholder="First Name"
                                      label="First Name"
                                      value={userDetails.first_name}
                                      onChange={this.handleChanged("first_name")}/>
                                     <br/>
                           <Divider variant="middle" />
                           <TextField style={styles.lastname} 
                                      placeholder="Email"
                                      label="Email"
                                      type='email'
                                      value={userDetails.email}
                                      onChange={this.handleChanged("email")}/>
                                      <br/>
                           <Divider variant="middle" />
                           <TextField style={styles.lastname} 
                                      placeholder="Last Name"
                                      label="Last Name"
                                      value={userDetails.last_name}
                                      onChange={this.handleChanged("last_name")}/>
                                      <br/>
                           <Divider variant="middle" />
                           <TextField style={styles.lastname} 
                                      placeholder={this.randomPlaceholder()}
                                      label="Username"
                                      value={userDetails.display_name}
                                      onChange={this.handleChanged("display_name")}/>
                           <Divider variant="middle" />
                           <ListItem>
                           <ListItemText primary="Age"/>
                              <ListItemSecondaryAction>
                                <TextField select
                                           value={userDetails.age}
                                           onChange={this.handleSelect('age')}
                                           style={styles.select}
                                           InputProps={{
                                                endAdornment:<InputAdornment position="end">yrs</InputAdornment>}}>
                                                {AGE.map(option => (
                                                     <MenuItem key={option.value} 
                                                               value={option.value}>
                                                                     {option.label}
                                                     </MenuItem>
                                                                  ))}
                                 </TextField>
                               </ListItemSecondaryAction>
                            </ListItem>
                            <Divider variant="middle" />
                            <ListItem>
                            <ListItemText primary="Height"/>
                              <ListItemSecondaryAction>
                                <TextField select
                                           value={userDetails.height}
                                           onChange={this.handleSelect('height')}
                                           style={styles.select1}
                                           InputProps={{
                                                endAdornment:<InputAdornment position="end">cm</InputAdornment>}}>
                                                {HEIGHT.map(option => (
                                                        <MenuItem key={option.value} 
                                                                  value={option.value}>
                                                                        {option.label}
                                                        </MenuItem>
                                                                    ))}
                                </TextField>
                             </ListItemSecondaryAction>
                             </ListItem>
                             <Divider variant="middle" />
                             <ListItem>
                             <ListItemText primary="Body Type"/>
                               <ListItemSecondaryAction>
                                 <TextField select
                                            value={userDetails.body_type}
                                            onChange={this.handleSelect('body_type')}
                                            style={styles.select1}>
                                            {BODYTYPE.map(option => (
                                                      <MenuItem key={option.value} 
                                                                value={option.value}>
                                                                      {option.label}
                                                      </MenuItem>
                                                                  ))} 

                                 </TextField>
                               </ListItemSecondaryAction>
                              </ListItem>
                              <Divider variant="middle" />
                              <ListItem>
                              <ListItemText primary="Gender"/>
                                <ListItemSecondaryAction>
                                  <TextField select
                                             value={userDetails.gender}
                                             onChange={this.handleSelect('gender')}
                                             style={styles.selectSex}> 
                                             {GENDER.map(option => (
                                                     <MenuItem key={option.value} 
                                                               value={option.value}>
                                                                     {option.label}
                                                     </MenuItem>
                                                                  ))} 
                                   </TextField>     
          
                                 </ListItemSecondaryAction>
                               </ListItem>
                               <Divider variant="middle"/> 
                               <br/>
                               <ListItem>
                               <ListItemText style={styles.li}
                                             primary='Maximum Distance of Chat'
                                             secondary={
                                               <React.Fragment>
                                                  <Typography component="span"
                                                              variant="body2"
                                                              color="textPrimary">
                                                     {userDetails.chat_distance+'mi'}
                                                  </Typography>
                                               </React.Fragment>
                                             }/>
                                    <ListItemSecondaryAction>
                                      <Slider value={userDetails.chat_distance}
                                              min={1}
                                              max={100}
                                              step={1}
                                              aria-labelledby="slider-icon"
                                              onChange={this.handleSlide}
                                              style={styles.slide}
                                              thumb={<LensIcon style={{ color: '#2196f3' }} />}/> 
                                    </ListItemSecondaryAction>
                                 </ListItem> 
                                 <Divider variant="middle" />
                                 <ListItem>
                                 <ListItemText primary="Looking for" style={styles.li}/>
                                    <ListItemSecondaryAction>
                                       <TextField select
                                                  value={userDetails.chat_gender}
                                                  onChange={this.handleSelect('chat_gender')}
                                                  style={styles.selectSex}>
                                                  {CHAT_GENDER.map(option => (
                                                          <MenuItem key={option.value} 
                                                                    value={option.value}>
                                                                          {option.label}
                                                          </MenuItem>
                                                                      ))}
                                       </TextField>
                                    </ListItemSecondaryAction>
                                 </ListItem>
                                 <Divider variant="middle" />
                                 <ListItem>
                                 <ListItemText primary="City"/>
                                    <ListItemSecondaryAction>
                                       <TextField style={styles.username}/>
                                    </ListItemSecondaryAction>
                                 </ListItem> 
                                 <Divider variant="middle" />
                                 <ListItem>
                                 <ListItemText primary="Hookup/Escort" style={styles.li}/>
                                    <Switch checked={userDetails.hookup}
                                            onChange={this.handleCheck('hookup')}
                                            style={styles.interes}
                                            value={userDetails.hookup}
                                            color="secondary"/>
                                 </ListItem>
                                 <Divider variant="middle" />
                                 <ListItem>
                                 <ListItemText primary="Dating" style={styles.li}/>
                                    <ListItemSecondaryAction>
                                       <Switch checked={userDetails.dating}
                                               onChange={this.handleCheck('dating')}
                                               style={styles.interes}
                                               value={userDetails.dating}
                                               color="secondary"/>
                                    </ListItemSecondaryAction>
                                 </ListItem>
                                 <Divider variant="middle" />
                     
                                <TextField id="filled-multiline-static"
                                           label="Description"
                                           placeholder="Write about yourself"
                                           multiline
                                           rows="4"
                                           value={userDetails.description}
                                           margin="normal"
                                           variant="outlined"
                                           style={styles.desc}
                                           onChange={this.handleChanged('description')}/>
        
        </List>
        </div>
         </form>
         </div>
         </Grid>
         </Grid>
         </div>
    )
   
  }
}
const styles={
  appbar:{
    color:"inherit",
    background:" linear-gradient(0.25turn, #e91e63, #f06292)",
    fontWeight: "bold",
   
},
paper:{
   backgroundColor:"transparent",
   width:'66vw',
   height:'100vh'
},
typo1:{
  marginLeft:"auto",
  fontFamily: 'yatra one regular',
  fontWeight: "bold",
  fontSize:39,
  marginRight:'auto',
},
button:{
  marginLeft:"auto",
  fontFamily: 'yatra one regular',
  fontWeight: "bold",
  fontSize:24,
},
username:{
width:300,

},
lastname:{
  width:300,
  
},
slide:{
  width:120,

},
select:{
  width:70,
 
  
  },
  select1:{
    width:70,
   
    
    },
    selectSex:{
      width:150,
    
      
      },


}

const mapStateToProps= (state)=>({
  userDetails:state.auths.userDetails,
  register: state.auths.register
})

export default connect(mapStateToProps, {addUserInfo})(UserInfo)