import React, { Component } from 'react'

export default class MessageInput extends Component {
  constructor(props){
     super(props)
     this.state = {
      // message:"",
      // isTyping:false
     };
  }

  /* handleSubmit = (e) =>{
      e.preventDefault()
      this.sendMessage()
      this.setState({message:""})
  } */

  /* sendMessage = () => {
    this.props.sendMessage(this.state.message)
  } */

  /* componentWillUnmount(){
    this.stopCheckingTyping()
  } */
  sendTyping = () => {
     this.lastUpdateTime = Date.now()
     if(!this.state.isTyping){
       this.setState({isTyping:true})
       this.props.sendTyping(true)
       this.startCheckingTyping()
     }
  }

  /* startCheckingTyping = () => {
    console.log("Typing");
    this.typingInterval = setInterval(() =>{
      if((Date.now() - this.lastUpdateTime) > 300)  {
        this.setState({isTyping:false})
        this.stopCheckingTyping()
      }
    },300)
  } */

 /*  stopCheckingTyping = () =>{
    console.log("Stop Typing")
    if(this.typingInterval){
        clearInterval(this.typingInterval)
        this.props.sendTyping(false)
    } */
 // }

 render() {
    const {message, messageHandler,renderMesages} = this.props
    return (
      <div className="message-input">
         <form
             onSubmit = {(e) => messageHandler(e, message)}
             className="message-form">

        <input
            style={{backgroundColor:"transparent",borderRadius:25,width:650,height:50,outline:"none",borderColor:"#e91e63"}}
            onChange = {messageHandler}
            id="message"
            ref={"messageinput"}
            type="text"
            className="form-control"
            value = {message}
            autoComplete={'off'}
            placeholder="Type a message"
            //onKeyUp={e => {e.keyCode !== 13 && this.sendTyping()}}


            />
        <button
          //  style={{backgroundColor:"transparent",width:100,height:50,borderRadius:30,outline:"none",borderColor:"#e91e63"}}
            //disabled = {message.length===0}
            type = "submit"
            className = "send"
            //value='submit'
            >
            Send
            </button>
        </form>
      </div>
    )
  }
}
