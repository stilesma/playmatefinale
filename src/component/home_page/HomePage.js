import React, { Component } from 'react'
import ControlPage from '../chat_page/ControlPage'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Button from '@material-ui/core/Button'
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos'
import ChatHeading from '../chat_page/ChatHeading'
//import SearchList from '../chat_page/SearchList'
import Chat from '../chat_page/Chat/Chat';
import {connect} from 'react-redux'

 class HomePage extends Component {
   constructor(props) {
    super(props);
    this.state = {
      loggedIn:false
    }
  }


   render() {
     //const {loggedIn} = this.state
    return (


       <div style={{ color:"#cac8e",
      display:"flex",
      flexDirection:"row",
      alignItems:"flex-start",
      height:"100%",
      width:"400px"}}>
      <ControlPage/>
      <div className="chat-room-container" style={{marginLeft:10,width:870}}>
                       {

                  <div className="chat-room" style={{width:800,backgroundColor:"#ffffff",height:650}}>
                  <AppBar position="relative" elevation={10} style={styles.appbar}>
                  <Toolbar><Button onClick={this.back}><ArrowBackIosIcon/></Button><ChatHeading /></Toolbar>
                  </AppBar>

                            <Chat/>
                           </div>

                        }
                     </div>

     </div>
    )
  }
}

 const styles= {
  appbar:{
    background:" linear-gradient(0.25turn, #e91e63, #f06292)",
    height:70,
    width:800
},
container:{
  color:"#cac8e",
  display:"flex",
  flexDirection:"row",
  alignItems:"flex-start",
  height:"100%",
  width:600
},
grid:{
  width:700,
},
}
function mapStateToProps(state) {
   const { chat } = state.chat;
  return {
    chat
  }; 
}
const connectedHomepage = connect(mapStateToProps)(HomePage); //IS THIS THE CONNECT U ARE TALKING ABOUt
export { connectedHomepage as HomePage };