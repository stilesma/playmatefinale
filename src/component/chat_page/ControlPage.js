import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Tab from '@material-ui/core/Tab'
import Tabs from '@material-ui/core/Tabs'
import Typography from '@material-ui/core/Typography'
import AppBar from '@material-ui/core/AppBar'
import PhoneIcon from '@material-ui/icons/Phone'
import ChatIcon from '@material-ui/icons/Chat'
import FilterListicon from '@material-ui/icons/FilterList'
import './controlpage.css'


function TabContainer({ children, dir }) {
    return (
      <Typography component="div" dir={dir} style={{ padding: 0,}}>
        {children}
      </Typography>
    );
  }
  
  TabContainer.propTypes = {
    children: PropTypes.node.isRequired,
    dir: PropTypes.string.isRequired,
  };

export class ControlPage extends Component {
    state = {
        value: 0,
      };
      handleChange = (event, value) => {
        this.setState({ value });
      };
  render() {
      const {value}= this.state;
    return (
        <div className= "sam"style={{width:450,  height:650,backgroundColor:"#ffffff",padding:0}}>
        <AppBar position="static" style={styles.appbar} color="inherit">
          <Tabs value={value} onChange={this.handleChange} style={styles.tabs}>
            <Tab label="Chat" icon={<ChatIcon/>} />
            <Tab label="Match List" icon={<FilterListicon/>} />
            <Tab label="Calls" icon={<PhoneIcon/>} disabled/>
          </Tabs>
        </AppBar>
        {value === 0 && <TabContainer style={styles.sidebar}> 
          <div id="side-bar">
        <div
          className="users"
          ref='users'>
        
          </div>
          </div>
                
            
      
   
        </TabContainer>}
        {value === 1 && <TabContainer>Item Two</TabContainer>}
        {value === 2 && <TabContainer>Item Three</TabContainer>}
        </div>
    )
  }
}
const styles = {
    appbar:{
        background:" linear-gradient(0.25turn, #e91e63, #f06292)",
        
    },
    sidebar:{
      width:"450px"
    },
    tabs:{
      width:"450px",
      height:"100%",
      display:"flex",justifyContent:"space-around",
      flexDirection:"column"
    }
}
export default ControlPage
