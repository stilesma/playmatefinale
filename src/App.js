import React from 'react';
import './App.css'
import {Provider} from 'react-redux'
import Profile from './test/components/home/Profile'
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'

import PrivateRoute from './test/components/PrivateRoute'
import ValidatePhone from './test/components/auths/ValidatePhone'
import Register from './test/components/auths/Register'
import Login from './test/components/auths/Login'
import store from "./test/store";
import UserInfo from './test/components/auths/UserInfo';
import { loadUser } from './test/actions/auths';
import { Layout } from './test/components/home/chats/Layout';
import { AppBar, Typography, Grid } from '@material-ui/core';
import Settings from './test/components/home/Settings';
import './test/components/auths/editr.css'
 
export default class App extends React.Component {
  componentDidMount(){
    store.dispatch(loadUser())
  }
  render(){
    return (
  <div className="App-header">
    <Grid container xs={12}>
      <div className="edit">
      <Provider store={store}>
        
        <Router>
          <Switch>
            <PrivateRoute exact path="/userdetails" component={UserInfo}/>
            <PrivateRoute exact path="/" component={Layout} />
            <Route exact path="/register" component={Register} />
            <Route exact path="/login" component={Login} />
            <Route exact path="/validatephone" component={ValidatePhone} /> 
            <PrivateRoute exact path="/profilepage" component={Profile}/>
            <PrivateRoute exact path="/settings" component={Settings}/>
          </Switch>
        </Router>
        
       </Provider>
      {/* <footer>
        <AppBar position='fixed' style={styles.appbar} >
          <Typography color='inherit' variant='title'>
            Designed by Stiles M.A
          </Typography>
        </AppBar>
      </footer>  */}
      </div>
      </Grid>
  </div>
     
    );
  }
  
}
const styles = {
  appbar:{
    top: 'auto',
    backgroundColor:'transparent',
      bottom: 0,
      width:"100vw",
    //background:" linear-gradient(0.25turn, #e91e63, #f06292)",
    color:'#ffffff'
  }
}

