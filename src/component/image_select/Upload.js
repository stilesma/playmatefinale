import React, {Component} from 'react'
import { userActions } from '../../redux/actions';
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import { Button } from '@material-ui/core';
import ArrowBackIosRoundedIcon from '@material-ui/icons/ArrowBackIosRounded'
import jerry from './jerry.jpg'

export default class Upload extends Component {
    constructor(props){
        super(props)
        this.state = {
            title: '',
            content:'',
            image:[]
        };
    }
        handleChange = (e) => {
            this.setState({
                [e.target.id]: e.target.value
            })
        };

        handleImageChange = (e) => {
            this.setState({
                image: e.target.files[0]
            })
        };

        handleSubmit = (e) => {
            const {image,content,title} = this.state
            const {dispatch} = this.props
            e.preventDefault();
            console.log(this.state);
            if (image && content && title) {
                dispatch(userActions.add_image(image,content,title))
            }
        }
    
    render() {
        const {image} = this.state
        return (
            <div>
                 <AppBar position="static" style={styles.appbar}>
                    <Toolbar>
                        <Button color='inherit'>
                        <ArrowBackIosRoundedIcon color='inherit'/>
                        </Button>
                       <Typography variant="headline" color="inherit" style={styles.typo1}>
                             Upload Images
                       </Typography>
                   </Toolbar>
                 </AppBar>
              <form onSubmit={this.handleSubmit}>
                <h2>dsff</h2>
              </form>
              <div>
                  {
                      image.length>1
                      ?
                      null 
                      :
                   <div style={{display:'flex', marginLeft:350}}>
                      
                      <div className="frame"
                           style=
                           {
                               {
                                position:'relative',
                              
                               }
                           }
                           >
                          <div className="present"
                               style=
                                 {{
                                    width:400,
                                    height:400,
                                    backgroundColor:'#fafafa',
                                    backgroundImage:"url('src/image_select/jerrylivewallpapercartoon1091311s307x512.jpg')",
                                    marginRight:'auto',
                                    marginLeft:'auto',
                                    display:'flex'
                                 }}
                          >
                              <img src={jerry} alt="jerry" 
                                   style={{position:'absolute',maxWidth:400, maxHeight:400,backgroundPosition:'center center',height:400, width:400}}/>
                          <Button/>
                      <Button
                          style={styles.button}/>
                      
                          </div>
                      </div>
                      
                   </div>
                  }

              </div>
            </div>
        )
    }
}

const styles = {
    appbar:
         {
           color:"inherit",
           background:" linear-gradient(0.25turn, #e91e63, #f06292)" ,
           fontWeight: "bold",
           fontSize:30,
           fontStretch:10,
          
         },
    button:
         {
            marginLeft:'auto'
         }
}