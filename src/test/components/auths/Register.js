import React, { Component } from 'react'
import PropTypes from 'prop-types'

import {connect} from 'react-redux'
import {Redirect} from "react-router-dom"
import 'react-phone-number-input/style.css'
import {register} from '../../actions/auths'
import './phone.css'
import AppBar from '@material-ui/core/AppBar'
import Button from '@material-ui/core/Button'
import Paper from '@material-ui/core/Paper'
import Toolbar from '@material-ui/core/Toolbar'
import Typography  from '@material-ui/core/Typography'
import TextField from '@material-ui/core/TextField'
import PhoneInput from 'react-phone-number-input' 

class Register extends Component {
  static propTypes = {
    register: PropTypes.func.isRequired,
    isAuthenticated: PropTypes.bool,
    phone_number: PropTypes.string,
    validated: PropTypes.bool
  }


   state = {
     phone_number: '',
     otp: '',
     open: false,
     open1: false,
   }

   onSubmit = e => {
     e.preventDefault()
     this.props.register(this.state.phone_number, this.state.otp)
   }

   handleClickOpen = () => {
    this.setState({ open: true })
    };
 
    handleClickOpen1 = (e) => {
     this.setState({ open1: true });
     this.setState({open: false})
     
   };
 
   handleClose = () => {
     this.setState({ open: false });
   };

   handleChange = e => {
    this.setState({[e.target.name]: e.target.value})
   }

   componentDidMount(){
     this.setState({phone_number: this.props.phone_number})
   }
  render() {
      if(this.props.isAuthenticated){
      return <Redirect to="/" />
    }
    if (this.props.validated === false) {
      return <Redirect to="/validatephone" />;
    }
    if (this.props.login) {
      return <Redirect to="/login" />;
    }  
     const {otp} = this.state
    return (
      <div>
         <AppBar position="static" style={styles.appbar}>
              <Toolbar>
                 <Typography variant="h3" color="inherit" style={styles.typo}>
                 Playmate
                 </Typography>
              </Toolbar>
          </AppBar>
          <Paper style={styles.paper}>
         <form onSubmit={this.onSubmit}>
           <PhoneInput
                name="phone_number"
                className="phone"
                style={styles.phone}
                placeholder="Enter Phone Number"
                value={this.props.phone_number}/> 
            {/* <input name="phone_number" type="tel" value={this.props.phone_number} readOnly/> */} 
            <br/>
            <TextField name="otp"
                        onChange={this.handleChange}
                        value={otp}
                        variant="outlined"
                        placeholder="OTP"
                        label="OTP"/>
              <br/>
              <Button style={styles.button} type="submit" >Submit</Button>
         </form>
         </Paper>
      </div>
    )
  }
}

const styles ={
  appbar:{
    color:"inherit",
    background:" linear-gradient(0.25turn, #e91e63, #f06292)" ,
    fontWeight: "bold",
    fontSize:30,
    fontStretch:10,
  },
  phone:{
    position:'relative',
    width:260,
    backgroundColor:'none',
    display:'inline-block',
    marginTop: 50,
  },
  paper:{
    width: 300,
    height: 300,
    marginLeft: 'auto',
    marginRight: 'auto',
    textAlign: 'center',
    marginTop: 100,
   
  },
  button:{
    marginTop:30,
    background:" linear-gradient(0.25turn, #e91e63, #f69d3c)",
    borderRadius:20,
    color:"inherit",
    width:200,
    height:40,
    fontFamily: 'georgia',
    fontWeight: "bold",
    fontSize:20,
    padding:-10
  },
  typo:{
    color:"#ffffff",
    marginLeft:'auto',
    marginRight:'auto'
  }
 }

 const mapStateToProps = state => ({
   isAuthenticated: state.auths.isAuthenticated,
   phone_number: state.auths.phone_number,
   validated: state.auths.validated,
   login: state.auths.login
 });
export default connect(mapStateToProps, {register})(Register)
