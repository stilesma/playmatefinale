import {
  USER_LOADED, 
  USER_LOADING, 
  AUTH_ERROR, 
  LOGIN_SUCCESS, 
  LOGIN_FAILURE, 
  LOGOUT_SUCCESS,
  REGISTER_SUCCESS,
  REGISTER_FAILURE,
  VALIDATE_SUCCESS,
  VALIDATE_FAILURE,
  VALIDATING,
  ADD_USER_INFO_FAILURE,
  ADD_USER_INFO_SUCCESS,
  UPDATE_USER_INFO_FAILURE
} from '../actions/types';

const initialState = {
  token: localStorage.getItem('token'),
  isAuthenticated: null,
  isLoading:false,
  validated: false,
  user: null,
  phone_number: null,
  register: false,
  login: false,
  

}
const user = {
  user: null
}

export default function (state = initialState, action) {
  switch(action.type){
      case USER_LOADING:
          return {
              ...state,
              isLoading: true
          }
      case USER_LOADED:
          return {
              ...state,
              isAuthenticated: true,
              isLoading: false,
              user: action.payload
          }
      case VALIDATING:
        return {
          ...state,
          isLoading: true
        }
      case VALIDATE_SUCCESS:
        return {
          ...state,
          isAuthenticated: false,
          isLoading: false,
          validated: true,
          phone_number: action.payload['phone_number'],
          register:action.payload['register'],
          login:action.payload['login']
        }
      case LOGIN_SUCCESS:
          localStorage.setItem('token', action.payload.token)
          return {
              ...state,
              ...action.payload,
              isAuthenticated: true,
              isLoading: false,
              login:true
          }
      case REGISTER_SUCCESS:
              
              localStorage.setItem('token', action.payload.token)
              return {
                  ...state,
                  ...action.payload,
                  isAuthenticated: true,
                  isLoading: false,
                  register:true
              }
      case ADD_USER_INFO_SUCCESS:
         return {
           ...state,
              userDetails:action.payload
         }
      case AUTH_ERROR:
      case LOGIN_FAILURE:
      case LOGOUT_SUCCESS:
      case VALIDATE_FAILURE:
      case REGISTER_FAILURE:
          localStorage.removeItem('token')
          return {
              ...state,
              token: null,
              user: null,
              isAuthenticated: false,
              isLoading: false,
              phone_number: null,
              validated: false,
              register: false,
              login: false
          }
     case ADD_USER_INFO_FAILURE: 
       return state

    case UPDATE_USER_INFO_FAILURE:
      return {
        ...state,

      }
    
      default:
        return state
  }
}

