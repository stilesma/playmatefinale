import {
  combineReducers
} from 'redux'

import auths from './auths'
import chats from './chats'

export default combineReducers({
  auths,
  chats
})